import { Component, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from '../shared/base.component';
import { PrvCity } from '../city/PrvCity';
import { Router } from '@angular/router';
import { SrvUtil } from 'app/shared/SrvUtil';
import { Type } from 'app/shared/MessageDialogObject';

@Component({
	selector: 'cmp-utility',
	templateUrl: 'CmpUtility.html',
	styleUrls: ['./CmpUtility.scss']
})

export class CmpUtility extends BaseComponent implements AfterViewInit {
	requestIndicatorClicked = false;
	errorMsg = '';

	constructor(private translate: TranslateService,
		private prvCity: PrvCity,
		private srvUtil: SrvUtil,
		private router: Router) {
		super();
	}

	ngAfterViewInit() {
	}

	showRequestIndicator() {
		this.requestIndicatorClicked = true;
		this.PageUtil.showRequestIndicator();
	}

	hideRequestIndicator() {
		this.requestIndicatorClicked = false;
		this.PageUtil.hideRequestIndicator();
	}

	showAlertSuccess() {
		this.PageUtil.showAlertSuccess(this.translate.instant('alert.success'));
		//to hide alert, call this.PageUtil.hideAlert();
	}

	showAlertDanger() {
		this.PageUtil.showAlertDanger(this.translate.instant('alert.danger'));
	}

	showAlertWarning() {
		this.PageUtil.showAlertWarning(this.translate.instant('alert.warning'));
	}

	showAlertInfo() {
		this.PageUtil.showAlertInfo(this.translate.instant('alert.info'));
	}

	errorNoParam() {
		this.prvCity.errorNoParam().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	errorWithParam() {
		this.prvCity.errorWithParam().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	unexpectedError() {
		this.prvCity.unexpectedError().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	unauthorizedError() {
		this.prvCity.unauthorizedError().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	showDialogInfo() {
		this.srvUtil.showDialogInfo('This is an info dialog. Please hit OK button to close it');
	}
	
	showDialogError() {
		this.srvUtil.showDialogError('This is an error dialog. Please hit OK button to close it. With relatively long message. This error should wrap gracefully. Otherwise, it is not good.');
	}

	async showDialogConfirm() {
		let answer = await this.srvUtil.showDialogConfirm('This is a confirmation dialog. Please hit yes or no');
		this.srvUtil.showDialogInfo('You just selected a ' + answer + ' answer')
	}
	
	showDialogCustom() {
		this.srvUtil.showDialog('This is a custom dialog. The <b>buttons</b> have custom labels.', 'Custom Title', Type.INFO, 'Go Ahead', 'Go Back', 'Stay Here')
		.then(answer => {
			console.log('answer: ' + answer);
			if (answer == 'yes') {
				this.srvUtil.showDialogInfo(`"Go Ahead" was the label for <b>${answer}</b> answer`);
			} else if (answer == 'no') {
				this.srvUtil.showDialogInfo(`"Go Back" was the label for <b>${answer}</b> answer`);
			} else if (answer == 'cancel') {
				this.srvUtil.showDialogInfo(`"Stay Here" was the label for <b>${answer}</b> answer`).then((answer2) => console.log(answer2));
			}
		});
	}
}
