import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from '../shared/base.service';
import {TableQuery} from '../shared/TabelQuery';

@Injectable()
export class PrvCountry extends BaseService {
	constructor(private http: HttpClient) {
		super();
	}

	find(start: number, max: number, tq: TableQuery) {
		return this.http.post(this.apiUrl + 'country?start=' + start + '&max=' + max, JSON.stringify(tq));
	}

	findById(id: number) {
		return this.http.get(this.apiUrl + 'country/' + id);
	}

	save(id: number, data: any) {
		return this.http.put(this.apiUrl + 'country/' + id, JSON.stringify(data));
	}
	deleteRecord(id) {
		return this.http.delete(this.apiUrl + 'country/' + id);
	}
	exportData(start: number, max: number, tq: TableQuery) {
		return this.http.post(this.apiUrl + 'country/xls?start=' + start + '&max=' + max, JSON.stringify(tq), {
			responseType: 'blob'
		});
	}
}

