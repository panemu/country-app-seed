import {NgModule} from '@angular/core';
import {CmpCountryList} from './CmpCountryList';
import {CmpCountryForm} from './CmpCountryForm';
import {RoutCountry} from './RoutCountry';
import {MdlShared} from '../shared/MdlShared';
import {PrvCountry} from './PrvCountry';

@NgModule({
	imports: [RoutCountry, MdlShared],
	declarations: [CmpCountryList, CmpCountryForm],
	providers:[PrvCountry]
})
export class MdlCountry {}