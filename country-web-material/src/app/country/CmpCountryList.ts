import { Component, OnInit, ViewChild } from '@angular/core';
import { PrvCountry } from './PrvCountry';
import { Router, ActivatedRoute } from '@angular/router';
import { TableQuery } from '../shared/TabelQuery';
import { BaseListComponent } from '../shared/BaseListComponent';

import { MatDialog, MatDialogRef, MatMenuTrigger } from '@angular/material';
import { CmpCountryForm } from '../country/CmpCountryForm';
import { TranslateService } from '@ngx-translate/core';
import { SrvMasterData } from '../shared/SrvMasterData';
import { SrvUtil } from 'app/shared/SrvUtil';

@Component({
	templateUrl: './CmpCountryList.html',
})

export class CmpCountryList extends BaseListComponent implements OnInit {
	countryForm: MatDialogRef<CmpCountryForm>;
	selected = [];
	noRowSelected = true;
	tableHeight: string;

	saving = false;
	insertMode = false;
	canMaintain = false;

	// @ViewChild('tableRowMenuTrigger', { static: false }) tableRowMenuTrigger: MatMenuTrigger;

	constructor(private prvCountry: PrvCountry,
		router: Router,
		route: ActivatedRoute,
		private dialog: MatDialog,
		public srvMasterData: SrvMasterData,
		srvUtil: SrvUtil,
		translate: TranslateService) {
		super('CmpCountryList', translate, route, router, srvUtil);
		this.canMaintain = this.srvMasterData.hasPermission('country:write');
		this.criteria = {
			name: { value: '', label: 'country.name' },
			capital: { value: '', label: 'capital.city' },
			continent: { value: '', label: 'continent' },
			independence: { value: '', label: 'independence.day' },
			population: { value: '', label: 'population' },
		};
	}

	openDialog(paramId: number): void {

		CmpCountryForm.showAsDialog(this.dialog, paramId).then((result: any) => {
			console.log('dialog returned ' + JSON.stringify(result));
			if (result) {
				this.PageUtil.showAlertSuccess(this.translate.instant('data.saved'));
				this.reload();
			}
		});

	}

	ngOnInit() {
		this.tableHeight = 'calc(100vh - 250px)';
		super.defaultOnInit();
	}

	edit() {
		if (this.selected && this.selected.length > 0) {
			this.openDialog(this.selected[0].id);
		}
	}
	add() {
		this.openDialog(0);
		// this.tableRowMenuTrigger.openMenu();
	}

	deleteSelected() {
		if (this.selected && this.selected.length > 0) {
			this.srvUtil.showDialogConfirm(
				'<strong>' + this.translate.instant('delete.confirmation')).then(
				(answer: string) => {
					if (answer === 'yes') {
						this.PageUtil.showRequestIndicator();
						this.prvCountry.deleteRecord(this.selected[0].id).subscribe(data => this.reload(), error => {
							this.srvUtil.handleError(error, this.router);
						});
					}
				}
			);
		}
	}

	getData(query: TableQuery) {
		return this.prvCountry.find(this.paginationObject.startIndex, this.paginationObject.maxRows, query);
	}

	exportData() {
		let query = this.getQueryObject();
		this.prvCountry.exportData(this.paginationObject.startIndex, this.paginationObject.maxRows, query).subscribe((res) => {
			window['saveAs'](res, 'country2.xlsx');
		}, error => {
			this.srvUtil.handleError(error, this.router);
		});
	}

	showSearch(pnlSearch) {
		this.dialog.open(pnlSearch);
	}
}
