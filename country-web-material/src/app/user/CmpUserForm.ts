import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../shared/base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../shared/user.service';
import { NgForm } from '@angular/forms';
import { SrvMasterData } from '../shared/SrvMasterData';
import { TranslateService } from '@ngx-translate/core';
import { AssetToUpload } from 'app/shared/AssetToUpload';
import { PrvAsset, ProgressInfo } from 'app/shared/PrvAsset';
import { AssetManager, PersistedAsset } from 'app/shared/AssetManager';
import { Config } from 'app/shared/config/env.config';
import { CmpFileUpload } from 'app/shared/CmpFileUpload';
import { MatDialogRef, MatDialog } from '@angular/material';
import { SrvUtil } from 'app/shared/SrvUtil';

class Model {
	id: number;
	username: string;
	idRole: number;
	email: string;
	password: string;
	version: number;
	profilPic: PersistedAsset;
	ktpPic: PersistedAsset;
	documents: PersistedAsset[];
	role: any;
}

interface ValidationItem {key: string; param?: any}
class FormError {
	username: ValidationItem = null;
	idRole: ValidationItem = null;
	email: ValidationItem = null;
	password: ValidationItem = null;
	password2: ValidationItem = null;
}

const CAMERA_PNG = 'assets/cam.png';

@Component({
	templateUrl: './CmpUserForm.html',
})

export class CmpUserForm extends BaseComponent implements OnInit {
	apiUrl = Config.API;
	assetManager: AssetManager;
	id: number;
	insertMode = false;
	model: Model;
	myForm: NgForm;
	@ViewChild('myForm', { static: false }) currentForm: NgForm;
	saving = false;
	lstRole: any[];
	password2: string;
	mySelf = false;
	// path_picture: string;
	profileMode = false;
	picUrl = CAMERA_PNG;
	ktpUrl = CAMERA_PNG;
	profilPicToUpload: AssetToUpload;
	ktpPicToUpload: AssetToUpload;
	progressInfo: ProgressInfo;
	dialogRef: MatDialogRef<CmpFileUpload>;

	constructor(private route: ActivatedRoute,
		private prvUser: UserService,
		private srvMasterData: SrvMasterData,
		private prvAsset: PrvAsset,
		private router: Router,
		private dialog: MatDialog,
		private srvUtil: SrvUtil,
		private translate: TranslateService) {
		super();

	}
	ngOnInit() {
		this.id = +this.route.snapshot.params['id'];
		this.assetManager = new AssetManager;
		this.assetManager.category = 'member';
		this.assetManager.referenceId = this.id;
		this.assetManager.subCategory = 'document';

		this.profileMode = this.route.snapshot.queryParams['mode'] === 'profile';
		this.insertMode = this.id === 0;
		if (!this.insertMode) {
			this.getData();
		} else {
			this.model = new Model;
		}

		this.prvUser.getRoles().subscribe(
			(data: any) => this.lstRole = data
		);
	}

	private getData() {
		this.PageUtil.showRequestIndicator();
		if (this.profileMode) {
			this.prvUser.getMyProfile().subscribe(
				(data: any) => {
					this.afterLoad(data);
				},
				(error) => this.srvUtil.handleError(error, this.router)
			);
		} else {
			this.prvUser.findById(this.id).subscribe(
				(data: any) => {
					this.afterLoad(data);
				},
				(error) => this.srvUtil.handleError(error, this.router)
			);
		}
	}

	afterLoad(data: any) {
		this.model = data;
		if (this.model.profilPic) {
			this.picUrl = this.apiUrl + 'asset/' + this.model.profilPic.category + '/' + this.model.profilPic.path;
		} else {
			this.picUrl = CAMERA_PNG;
		}

		if (this.model.ktpPic) {
			this.ktpUrl = this.apiUrl + 'asset/' + this.model.ktpPic.category + '/' + this.model.ktpPic.path;
		} else {
			this.ktpUrl = CAMERA_PNG;
		}
		this.assetManager.clear();
		this.assetManager.persistedAssets = this.model.documents;
		this.mySelf = this.srvMasterData.getUsername() === this.model.username;
		this.PageUtil.hideRequestIndicator();
	}

	onSubmit(controls) {
		if (this.saving) return;
		this.saving = true;
		let toSave = new Model;
		if (!this.insertMode) {
			toSave.id = this.id;
		}
		toSave.idRole = controls.idRole.value;
		toSave.username = controls.username.value;
		toSave.email = controls.email.value;
		if (controls.password.value || controls.password2.value) {
			if (controls.password.value !== controls.password2.value) {
				this.formErrors['password2'] = { key: 'same.password' };
				this.srvUtil.showDialogError(this.translate.instant('same.password'));
				this.saving = false;
				return;
			}
			toSave.password = controls.password.value;
		}
		if (this.insertMode) {
			this.PageUtil.showRequestIndicator();
			this.prvUser.create(toSave).subscribe(
				async data =>  {
					console.log('after insert: ' + data);
					this.id = +data;
					this.assetManager.referenceId = data;
					console.log("=======START INSERT=======");
					await this.savePersistedAssets();
					await this.deleteMarkedAssets();
					await this.uploadAssets();
					this.afterSubmitSuccess();
					console.log("=======END INSERT=======");
				},
				error => {
					this.srvUtil.handleError(error, this.router);
					this.saving = false;
				}
			);
		} else {
			this.PageUtil.showRequestIndicator();
			toSave.version = this.model.version;
			this.prvUser.update(toSave).subscribe(
				async (data) => {
					console.log("=======START SAVE=======");
					await this.savePersistedAssets();
					await this.deleteMarkedAssets();
					await this.uploadAssets();
					this.afterSubmitSuccess();
					console.log("=======END SAVE=======");
				},
				error => {
					this.srvUtil.handleError(error, this.router);
					this.saving = false;
				}
			);
		}
	}

	private async savePersistedAssets() {
		console.log('savePersistedAssets');
		if (this.assetManager.persistedAssets !== undefined && this.assetManager.persistedAssets.length > 0) {
			await this.prvAsset.saveAssets(...this.assetManager.persistedAssets).toPromise().catch(
				error => this.srvUtil.handleError(error, this.router)
			);
		}
	}

	private async deleteMarkedAssets() {
		console.log('deleteAssets');
		if (this.assetManager.assetsToDelete !== undefined && this.assetManager.assetsToDelete.length > 0) {
			await this.prvAsset.deleteAssets(...this.assetManager.assetsToDelete).toPromise().catch(
				error => this.srvUtil.handleError(error, this.router)
			);
		}
		// if (this.assetManager.assetsToDelete !== undefined && this.assetManager.assetsToDelete.length > 0) {
		// 	this.prvAsset.deleteAssets(...this.assetManager.assetsToDelete).subscribe(
		// 		result => this.uploadAssets(),
		// 		error => this.srvUtil.handleError(error, this.router)
		// 	);
		// } else {
		// 	this.uploadAssets();
		// }

	}

	private async uploadAssets() {
		console.log('uploadAssets');
		let lstAssetToUpload: AssetToUpload[] = [];
		if (this.profilPicToUpload) {
			lstAssetToUpload.push(this.profilPicToUpload);
			this.profilPicToUpload = null;
		}
		if (this.ktpPicToUpload) {
			lstAssetToUpload.push(this.ktpPicToUpload);
			this.ktpPicToUpload = null;
		}
		lstAssetToUpload = lstAssetToUpload.concat(this.assetManager.assetsToUpload);
		if (this.insertMode) {
			lstAssetToUpload.forEach(item => item.referenceId = this.id);
		}
		if (lstAssetToUpload.length > 0) {

			this.progressInfo = new ProgressInfo;
			this.showProgressDialog();
			await this.prvAsset.uploadAssets(lstAssetToUpload, this.progressInfo).toPromise().then(
				() => {
					window.setTimeout(() => {
						this.dialogRef.close();
					}, 2000);
				}
			);
		}
	}

	private afterSubmitSuccess() {
		console.log('after submit success');
		this.PageUtil.hideRequestIndicator();
		this.saving = false;
		this.PageUtil.showAlertSuccess(this.translate.instant('data.saved'));
		this.password2 = '';
		if (this.insertMode) {
			this.insertMode = false;
			this.router.navigate(['../' + this.id], { relativeTo: this.route });
		}
		this.getData();
	}

	ngAfterViewChecked() {
		this.formChanged();
	}

	formChanged() {
		if (this.currentForm === this.myForm) { return; }
		this.myForm = this.currentForm;
		if (this.myForm) {
			this.myForm.valueChanges
				.subscribe(data => this.onValueChanged(data));
		}
	}

	onValueChanged(data?: any) {
		if (!this.myForm) { return; }
		const form = this.myForm.form;

		for (const field of Object.keys(this.formErrors)) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);

			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key of Object.keys(control.errors)) {
					this.formErrors[field] = messages[key];
				}
			}
		}
	}

	deleteAsset(type?: string) {
		if (type === 'ktp') {
			if (this.model.ktpPic) {
				this.assetManager.assetsToDelete.push(this.model.ktpPic);
			}
			this.ktpPicToUpload = null;
			this.ktpUrl = 'assets/cam.png';
		} else {
			if (this.model.profilPic) {
				this.assetManager.assetsToDelete.push(this.model.profilPic);
			}
			this.profilPicToUpload = null;
			this.picUrl = 'assets/cam.png';
		}
	}

	onSelectFile(event, type?: string) {
		if (event.target.files && event.target.files[0]) {
			if (type === 'ktp') {

				const reader = new FileReader();

				reader.readAsDataURL(event.target.files[0]);
				reader.onload = (evt: any) => {
					this.ktpUrl = evt.target.result;
				}

				this.ktpPicToUpload = new AssetToUpload;
				this.ktpPicToUpload.category = 'member';
				this.ktpPicToUpload.subCategory = 'ktp';
				this.ktpPicToUpload.overwrite = true;
				this.ktpPicToUpload.referenceId = this.id;
				this.ktpPicToUpload.file = event.target.files[0];

			} else {
				
				const reader = new FileReader();

				reader.readAsDataURL(event.target.files[0]);
				reader.onload = (evt: any) => {
					this.picUrl = evt.target.result;
				}

				this.profilPicToUpload = new AssetToUpload;
				this.profilPicToUpload.category = 'member';
				this.profilPicToUpload.overwrite = true;
				this.profilPicToUpload.referenceId = this.id;
				this.profilPicToUpload.file = event.target.files[0];
			}
		}
	}

	showProgressDialog() {
		this.dialogRef = this.dialog.open(CmpFileUpload, {
			width: '500px',
			data: this.progressInfo,
			disableClose: true
		});

	}

	formErrors: FormError = new FormError();

	validationMessages = {
		username: {
			required: { key: 'this.field.is.required' },
			minlength: { key: 'min.char', param: { par1: '4' } },
			maxlength: { key: 'max.char', param: { par1: '255' } },
		},
		idRole: {
			required: { key: 'this.field.is.required' },
		},
		email: {
			required: { key: 'this.field.is.required' },
			pattern: { key: 'valid.email' },
		},
		password: {
			required: { key: 'this.field.is.required' },
			minlength: { key: 'min.char', param: { par1: '4' } },
		},
		password2: {
			required: { key: 'this.field.is.required' },
			minlength: { key: 'min.char', param: { par1: '4' } },
		}
	};
}