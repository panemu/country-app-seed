import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CmpLandingPage } from './CmpLandingPage';
import { CmpPublic } from './CmpPublic';
import { CmpAbout } from './CmpAbout';
import { RoutPublic } from './RoutPublic';
import { MdlShared } from 'app/shared/MdlShared';


@NgModule({
	imports: [RouterModule, RoutPublic],
	exports: [],
	declarations: [CmpPublic, CmpLandingPage, CmpAbout],
	providers: [],
})
export class MdlPublic { }
