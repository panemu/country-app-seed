import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '../not-found.component';
import { CmpPublic } from './CmpPublic';
import { CmpLandingPage } from './CmpLandingPage';
import { CmpAbout } from './CmpAbout';

@NgModule({
	imports: [RouterModule.forChild([
		{
			path: 'public', component: CmpPublic,
			children: [
				{ path: 'about', component: CmpAbout },
				{ path: '', component: CmpLandingPage },
			],
		},
		{path: '**', component: PageNotFoundComponent},
	])],
	exports: [RouterModule]
})
export class RoutPublic { }
