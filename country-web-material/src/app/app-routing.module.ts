import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './login/auth-guard.service';
import { PageNotFoundComponent } from './not-found.component';

export const routes: Routes = [
	{
		path: 'home',
		loadChildren: () => import('./home/MdlHome').then(mod => mod.MdlHome),
		canLoad: [AuthGuard]
	},
	{ path: '', redirectTo: '/public', pathMatch: 'full' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
