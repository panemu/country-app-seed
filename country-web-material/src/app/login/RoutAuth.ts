import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth-guard.service';
import { CmpLogin } from './CmpLogin';

const loginRoutes: Routes = [
	{ path: 'login', component: CmpLogin }
];

@NgModule({
	imports: [
		RouterModule.forChild(loginRoutes)
	],
	exports: [
		RouterModule
	],
	providers: [
		AuthGuard
	]
})
export class RoutAuth { }


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/