import { environment } from '../../../environments/environment';

export const Config = {
	API: environment.API,
	appName: environment.appName,
	production: environment.production
};

