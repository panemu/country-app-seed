import { OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageUtil } from './PageUtil';

declare var jQuery: any;

export class BaseComponent implements OnDestroy {
	protected PageUtil = PageUtil;
	constructor() { }

	ngOnDestroy() {
		PageUtil.hideRequestIndicator();
		PageUtil.hideAlert();
	}

	protected getDefaultParameter(route: ActivatedRoute, paramName: string) {
		return this.PageUtil.getDefaultParameter(route, paramName);
	}

	protected emptyStringIfUndefined(param: any): string {
		return param === undefined || param == null || param === 'null' ? '' : param;
	}

	protected defaultIfUndefined(param: any, def: string): string {
		return param === undefined || param == null || param === 'null' ? def : param;
	}

	protected zeroIfUndefined(param: any): number {
		return param === undefined ? 0 : +param;
	}

	protected booleanIfUndefined(param: any): boolean {
		if (param === 'false') {
			return false;
		} else if (param === 'true') {
			return true;
		}
		return param === undefined ? false : param;
	}

	expandCollapse(selector: string) {
		// jQuery(selector).collapse('toggle');
	}

	expandPanel(selector: string) {
		// jQuery(selector).collapse('show');
	}
	collapsePanel(selector: string) {
		// jQuery(selector).collapse('hide');
	}
}
