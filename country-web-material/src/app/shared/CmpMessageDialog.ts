import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MessageDialogObject } from './MessageDialogObject';

@Component({
	selector: 'cmp-message-dialog',
	templateUrl: './CmpMessageDialog.html',
	styleUrls: ['./CmpMessageDialog.scss']
})

export class CmpMessageDialog {
	titleClass = '';
	constructor(public dialogRef: MatDialogRef<CmpMessageDialog>,
		@Inject(MAT_DIALOG_DATA) public data: MessageDialogObject) {
		if (data.type === 0) {
			this.titleClass = 'mat-dialog-title bg-blue-600 text-white';
		} else if (data.type === 1) {
			this.titleClass = 'mat-dialog-title bg-yellow-600 text-white';
		} else if (data.type === 2) {
			this.titleClass = 'mat-dialog-title bg-red-600 text-white';
		}
		if (data.noLabel || data.cancelLabel) {
			this.dialogRef.disableClose = true;
		}
	}
	close(answer) {
		this.dialogRef.close(answer);
	}
}