import { Component, ElementRef, forwardRef, OnInit, OnDestroy, Input, Optional, Self } from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
import { FocusMonitor } from '@angular/cdk/a11y';

@Component({
	selector: 'string-date-picker',
	template: `
	<div class="wrapper">
	<input class="mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored" [matDatepicker]="picker" [(value)]="_innerValue" (dateChange)="_innerValueChange($event)" [disabled]="_disabled">
	<mat-datepicker-toggle matSuffix [for]="picker" ></mat-datepicker-toggle>
	<mat-datepicker #picker></mat-datepicker>
</div>
	`,
	providers: [{ provide: MatFormFieldControl, useExisting: StringDatePicker }],
	styleUrls: ['StringDatePicker.scss']
})

export class StringDatePicker implements OnInit, ControlValueAccessor, MatFormFieldControl<string>, OnDestroy {
	_value: string;
	_innerValue: Date;
	onChange = (_: any) => { };
	onTouched = () => { };
	// -----

	static nextId = 0;

	stateChanges = new Subject<void>();
	focused = false;
	errorState = false;
	controlType = 'string-date-picker';
	id = `string-date-picker-${StringDatePicker.nextId++}`;
	describedBy = '';

	constructor(private _elementRef: ElementRef,
		private _focusMonitor: FocusMonitor,
		@Optional() @Self() public ngControl: NgControl) {

		_focusMonitor.monitor(_elementRef, true).subscribe(origin => {
			if (this.focused && !origin) {
				this.onTouched();
			}
			this.focused = !!origin;
			this.stateChanges.next();
		});

		if (this.ngControl != null) {
			this.ngControl.valueAccessor = this;
		}
	}

	get empty() {
		return !this._value;
	}

	get shouldLabelFloat() { return this.focused || !this.empty; }

	@Input()
	get placeholder(): string { return this._placeholder; }
	set placeholder(value: string) {
		this._placeholder = value;
		this.stateChanges.next();
	}
	private _placeholder: string = '';

	@Input()
	get required(): boolean { return this._required; }
	set required(value: boolean) {
		this._required = coerceBooleanProperty(value);
		this.stateChanges.next();
	}
	private _required = false;

	@Input()
	get disabled(): boolean { return this._disabled; }
	set disabled(value: boolean) {
		this._disabled = coerceBooleanProperty(value);
		this.stateChanges.next();
	}
	_disabled = false;

	@Input()
	get value(): string | null {
		return this._value;
	}
	set value(val: string | null) {
		// console.log('stringdatepicker val: ' + val);
		this._value = val;
		this.stateChanges.next();
	}

	ngOnDestroy() {
		this.stateChanges.complete();
		this._focusMonitor.stopMonitoring(this._elementRef);
	}

	setDescribedByIds(ids: string[]) {
		this.describedBy = ids.join(' ');
	}

	onContainerClick(event: MouseEvent) {
		if ((event.target as Element).tagName.toLowerCase() != 'input') {
			this._elementRef.nativeElement.querySelector('input')!.focus();
		}
	}

	ngOnInit() {
		// const inputField = this.elRef.nativeElement.children[0];
		// inputField.className = this.elRef.nativeElement.className;
		// this.elRef.nativeElement.className = '';
	}

	writeValue(obj: any): void {
		// console.log('write value ' + obj);
		if (obj) {
			this._innerValue = new Date(obj);
			this.value = this.toIsoDate(this._innerValue);
		} else {
			this.value = '';
			this._innerValue = null;
		}
		this.onChange(this._value);
	}
	setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	private toIsoDate(d: Date): string {
		const dd = this._2digit(d.getDate());
		const mm = this._2digit(d.getMonth() + 1);
		const yy = d.getFullYear();
		const stringDate = yy + '-' + mm + '-' + dd;
		return stringDate;
	}

	_innerValueChange(evt: MatDatepickerInputEvent<Date>) {
		const newVal = evt.value;
		if (newVal) {
			this.value = this.toIsoDate(newVal);
		} else {
			this.value = '';
		}
		this.onChange(this._value);
	}

	private _2digit(n: number) {
		return ('00' + n).slice(-2);
	}

	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

	_handleInput(): void {
		this.onChange(this.value);
	}


	static ngAcceptInputType_disabled: boolean | string | null | undefined;
	static ngAcceptInputType_required: boolean | string | null | undefined;
}
