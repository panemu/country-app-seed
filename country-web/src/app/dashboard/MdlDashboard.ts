import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CmpDashboard} from './CmpDashboard';
import {MdlShared} from '../shared/MdlShared';

@NgModule({
	imports: [CommonModule, MdlShared],
	declarations: [CmpDashboard],
	exports: [CmpDashboard],
	providers: []
})

export class MdlDashboard {}
