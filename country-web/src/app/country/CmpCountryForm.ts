import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {BaseComponent} from '../shared/base.component';
import {Router} from '@angular/router';
import {PrvCountry} from './PrvCountry';
import {NgForm} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { SrvUtil } from 'app/shared/SrvUtil';

class Model {
	id: number;
	name: string;
	capital: string;
	continent: string;
	independence: string;
	population: number;
}

interface ValidationItem {key: string; param?: any}
class FormError {
	name: ValidationItem = null;
}

@Component({
	selector: 'country-form',
	templateUrl: './CmpCountryForm.html',
})

export class CmpCountryForm extends BaseComponent implements OnInit {
	id: number;
	insertMode: boolean = false;
	model: Model;
	myForm: NgForm;
	lstContinent = ['Asia', 'Europe', 'Africa', 'Oceania', 'North America', 'Antarctica', 'South America', 'Australia'];
	@ViewChild('myForm', { static: true }) currentForm: NgForm;
	saving = false;
	private promiseResult: (l: Model) => void;

	static showAsDialog(dialog: MatDialog, paramId: number): Promise<Model> {
		const dialogRef = dialog.open(CmpCountryForm, { disableClose: true, width: '600px', });
		return new Promise<Model>((resolve, reject) => {
			console.log('inside promise building');
			dialogRef.componentInstance.id = paramId;
			dialogRef.componentInstance.promiseResult = resolve;
		}).then((v) => {
			dialogRef.close();
			return v;
		});
	}

	constructor(
		private prvCountry: PrvCountry,
		private router: Router,
		private srvUtil: SrvUtil) {
		super();
	}
	ngOnInit() {
		console.log('onInit');
		this.insertMode = this.id === 0;
		if (!this.insertMode) {
			this.PageUtil.showRequestIndicator();
			this.prvCountry.findById(this.id).subscribe(
				(data: Model) => {
					this.model = data;
					this.PageUtil.hideRequestIndicator();
				},
				(error) => this.srvUtil.handleError(error, this.router)
			);
		} else {
			this.model = new Model;
			this.model.id = this.id;
		}
	}

	ngAfterViewChecked() {
		this.formChanged();
	}

	formChanged() {
		if (this.currentForm === this.myForm) {return;}
		this.myForm = this.currentForm;
		if (this.myForm) {
			this.myForm.valueChanges
				.subscribe(data => this.onValueChanged(data));
		}
	}

	onValueChanged(data?: any) {
		if (!this.myForm) {return;}
		const form = this.myForm.form;

		for (const field of Object.keys(this.formErrors)) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);

			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key of Object.keys(control.errors)) {
					this.formErrors[field] = messages[key];
				}
			}
		}
	}
	
	submit() {
		if (this.saving) return;
		this.PageUtil.showRequestIndicator();
		this.saving = true;
		this.prvCountry.save(this.model.id, this.model).subscribe(
			(data: any) => {
				this.PageUtil.hideRequestIndicator();
				this.saving = false;
				this.insertMode = false;
				this.promiseResult(this.model);
			},
			error => {
				this.srvUtil.handleError(error, this.router);
				this.saving = false;
			}
		)
	}

	onIndependenceChange(val) {
		console.log('independence changed ' + val);
	}
	
	close() {
		this.promiseResult(null);
	}
	
	formErrors = new FormError();

	validationMessages = {
		'name': {
			'required': {key: 'this.field.is.required'},
			'maxlength': {key: 'max.char', param: {par1: '255'}},
			'minlength': {key: 'min.char', param: {par1: '4'}},
		},
	};
}