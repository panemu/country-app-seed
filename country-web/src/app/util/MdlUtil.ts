import {NgModule} from '@angular/core';
import {MdlShared} from '../shared/MdlShared';
import {RoutUtil} from './RoutUtil';
import {CmpUtility} from './CmpUtility';
import {PrvCity} from '../city/PrvCity'


@NgModule({
	imports: [RoutUtil, MdlShared],
	declarations: [CmpUtility],
	providers: [PrvCity]
})
export class MdlUtil {}