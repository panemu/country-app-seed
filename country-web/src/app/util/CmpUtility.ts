import { Component, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from '../shared/base.component';
import { PrvCity } from '../city/PrvCity';
import { Router } from '@angular/router';
import { SrvUtil } from 'app/shared/SrvUtil';

@Component({
	selector: 'cmp-utility',
	templateUrl: 'CmpUtility.html',
	styleUrls: ['./CmpUtility.scss']
})

export class CmpUtility extends BaseComponent implements AfterViewInit {
	requestIndicatorClicked = false;
	errorMsg = '';

	constructor(private translate: TranslateService,
		private prvCity: PrvCity,
		private srvUtil: SrvUtil,
		private router: Router) {
		super();
	}

	ngAfterViewInit() {
	}

	showRequestIndicator() {
		this.requestIndicatorClicked = true;
		this.PageUtil.showRequestIndicator();
	}

	hideRequestIndicator() {
		this.requestIndicatorClicked = false;
		this.PageUtil.hideRequestIndicator();
	}

	showAlertSuccess() {
		this.PageUtil.showAlertSuccess('Success and Info Alerts are auto-close their selves.');
	}

	showAlertDanger() {
		this.PageUtil.showAlertDanger('Warning and Error alerts are not automatically closed. Click me to close.');
	}

	showAlertWarning() {
		this.PageUtil.showAlertWarning('Warning and Error alerts are not automatically closed. Click me to close.');
	}

	showAlertInfo() {
		this.PageUtil.showAlertInfo('Success and Info Alerts are auto-close their selves.');
	}

	errorNoParam() {
		this.prvCity.errorNoParam().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	errorWithParam() {
		this.prvCity.errorWithParam().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	unexpectedError() {
		this.prvCity.unexpectedError().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	unauthorizedError() {
		this.prvCity.unauthorizedError().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		);
	}

	showDialogInfo() {
		this.srvUtil.showDialogInfo('This is an info dialog. Please hit OK button to close it');
	}

	showDialogError() {
		this.srvUtil.showDialogError('This is an error dialog. Please hit OK button to close it. With relatively long message. This error should wrap gracefully. Otherwise, it is not good.');
	}

	async showDialogConfirm() {
		let answer = await this.srvUtil.showDialogConfirm('This is a confirmation dialog. Please hit yes or no');
		this.srvUtil.showDialogInfo('You just selected a ' + answer + ' answer')
	}

	showDialogCustom() {
		this.srvUtil.showDialog({
			type: 'confirm',
			content: 'My name is <b>Amrullah</b>! <input>',
			title: 'Custom Title',
			yesLabel: 'Go Ahead',
			noLabel: 'Go Back',
			cancelLabel: 'Stay Here'
		}).then(answer => {
			console.log('answer: ' + answer);
			if (answer === 'yes') {
				this.srvUtil.showDialogInfo(`"Go Ahead" was the label for <b>${answer}</b> answer`);
			} else if (answer === 'no') {
				this.srvUtil.showDialogInfo(`"Go Back" was the label for <b>${answer}</b> answer`);
			} else if (answer === 'cancel') {
				this.srvUtil.showDialogInfo(`"Stay Here" was the label for <b>${answer}</b> answer`).then((answer2) => console.log(answer2));
			}
		});
	}

	showDialogCustom2() {
		this.srvUtil.showDialog({content: 'Another way to display a message dialog'});
	}
}
