import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CmpLogin } from './CmpLogin';
import { RoutAuth } from './RoutAuth';
import { MdlShared } from 'app/shared/MdlShared';


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RoutAuth,
		MdlShared
	],
	declarations: [
		CmpLogin
	]
})
export class MdlAuth { }
