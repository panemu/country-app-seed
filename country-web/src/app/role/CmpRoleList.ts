import {Component, OnInit} from '@angular/core';
import {BaseListComponent} from '../shared/BaseListComponent';
import {Router, ActivatedRoute} from '@angular/router';
import {TableQuery} from '../shared/TabelQuery';
import {PrvRole} from './PrvRole';
import {TranslateService} from '@ngx-translate/core';
import {SrvMasterData} from '../shared/SrvMasterData';
import { SrvUtil } from 'app/shared/SrvUtil';
import { MatDialog } from '@angular/material/dialog';

@Component({
	templateUrl: './CmpRoleList.html',
})

export class CmpRoleList extends BaseListComponent implements OnInit {
	selected = [];
	tableHeight: string;
	canMaintain = false;
	showMenu = false;
	constructor(router: Router,
		route: ActivatedRoute,
		translate: TranslateService,
		private dialog: MatDialog,
		srvUtil: SrvUtil,
		private prvRole: PrvRole,
		public srvMasterData: SrvMasterData) {
		super('CmpRoleList', translate, route, router, srvUtil);
		this.canMaintain = srvMasterData.hasPermission('role:write');
		this.criteria = {
			name: {value: '', label: 'access.right'},
			description: {value: '', label: 'description'}
		};
	}

	ngOnInit() {
		this.tableHeight = 'calc(100vh - 180px)';
		this.defaultOnInit();
	}

	getData(query: TableQuery) {
		return this.prvRole.find(this.paginationObject.startIndex, this.paginationObject.maxRows, query);
	}

	exportData() {
		let query = this.getQueryObject();
		this.prvRole.exportData(this.paginationObject.startIndex, this.paginationObject.maxRows, query).subscribe((res) => {
			window['saveAs'](res, 'role.xlsx');
		}, error => {
			this.srvUtil.handleError(error, this.router);
		});
	}

	edit() {
		if (this.selected && this.selected.length > 0) {
			this.router.navigate(['./' + this.selected[0].id], {relativeTo: this.route});
		}
	}

	deleteSelected() {
		if (this.selected && this.selected.length > 0) {
			this.srvUtil.showDialogConfirm(
				'<strong>' + this.translate.instant('delete.confirmation')).then(
				(answer: string) => {
					if (answer === 'yes') {
						this.PageUtil.showRequestIndicator();
						this.prvRole.deleteRecord(this.selected[0].id, this.selected[0].version).subscribe(
							data => this.reload(),
							error => {
								this.srvUtil.handleError(error, this.router);
							});
					}
				});

		}
	}

	showSearch(pnlSearch) {
		this.dialog.open(pnlSearch);
	}
}