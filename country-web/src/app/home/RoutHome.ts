import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CmpHome} from './CmpHome';
import {CmpDashboard} from 'app/dashboard/CmpDashboard';
import {CmpHelp} from 'app/home/CmpHelp';
import {AuthGuard} from '../login/auth-guard.service';

@NgModule({
	imports: [RouterModule.forChild([
		{
			path: '', component: CmpHome,
			canActivate: [AuthGuard],
			children: [
				{path: 'user', loadChildren: () => import('app/user/MdlUser').then(m => m.MdlUser)},
				{path: 'role', loadChildren: () => import('app/role/MdlRole').then(m => m.MdlRole)},
				{path: 'country', loadChildren: () => import('app/country/MdlCountry').then(m => m.MdlCountry)},
				{path: 'city', loadChildren: () => import('app/city/MdlCity').then(m => m.MdlCity)},
				{path: 'util', loadChildren: () => import('app/util/MdlUtil').then(m => m.MdlUtil)},
				{path: 'help', component: CmpHelp},
				{path: '', component: CmpDashboard},
			]
		},
	])],
	exports: [RouterModule]
})
export class RoutHome {}
