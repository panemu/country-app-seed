import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { PageUtil } from '../shared/PageUtil';
import { Alert } from '../shared/CmpAlert';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { SrvMasterData } from '../shared/SrvMasterData';
import { BaseComponent } from '../shared/base.component';
import { TranslateService } from '@ngx-translate/core';
import { Config } from 'app/shared/config/env.config';
import { SrvUtil } from 'app/shared/SrvUtil';

declare var jQuery: any;

@Component({
	selector: 'cmp-home',
	templateUrl: 'CmpHome.html',
	styleUrls: ['./CmpHome.scss']
})

export class CmpHome extends BaseComponent implements OnInit, AfterViewInit {
	//-----------------
	mobileMenuShow = false;
	//-----------------
	apiUrl = Config.API;
	minified = false;
	username: string;
	role: string;
	nama: string;
	balance: any;
	userId: number;
	message: Alert;
	requesting: boolean;
	alertTimeout: any;
	saldo: number;
	testMode = false;
	loadingProgress = 15;
	lang: string;
	authInfo: any;

	canReadUser = false;
	canReadRole = false;
	canReadCountry = false;
	canReadCity = false;
	canMaintainCountry = false
	canMaintainCity = false
	prefix = 'CmpHome';
	main_menu_collapsed = false;
	private _intervalCounterId: any = 0;
	public interval = 500; // in milliseconds
	show = false;
	componentRef;

	constructor(private userService: UserService, private router: Router,
		private srvMasterData: SrvMasterData,
		private translate: TranslateService,
		private srvUtil: SrvUtil) {
		super();
		this.authInfo = JSON.parse(localStorage.getItem(Config.appName + '.' + 'auth_info'));
		this.canReadUser = this.srvMasterData.isAdmin()
			|| this.srvMasterData.hasPermission('user:read')
			|| this.srvMasterData.hasPermission('user:write');
		this.canReadRole = this.srvMasterData.isAdmin()
			|| this.srvMasterData.hasPermission('role:read')
			|| this.srvMasterData.hasPermission('role:write');
		this.canReadCountry = this.srvMasterData.hasPermission('country:read');
		this.canReadCity = this.srvMasterData.hasPermission('city:read');
		this.canMaintainCountry = this.srvMasterData.hasPermission('country:write');
		this.canMaintainCity = this.srvMasterData.hasPermission('city:write');
		this.srvMasterData.checkAuth().subscribe(
			data => { },
			error => this.srvUtil.handleError(error, this.router)
		)
	}

	ngOnInit() {
		let intViewportWidth = window.innerWidth;
		console.log('cmphome on init ' + intViewportWidth);
		this.lang = this.translate.getDefaultLang();
		this.username = this.srvMasterData.getUsername();
		this.role = this.srvMasterData.getRole();
		this.nama = this.srvMasterData.getName();
		this.userId = this.srvMasterData.getUserId();
		this.testMode = this.srvMasterData.isTestMode();
		this.minified = this.srvMasterData.getDefaultParameter(this.prefix, null, 'minified') === 'true';

		PageUtil.getAlertObservable().subscribe(data => {
			this.message = data;
			if (this.alertTimeout) {
				clearTimeout(this.alertTimeout);
			}
			this.alertTimeout = undefined;
			if (this.message && (this.message.type === 'success' || this.message.type === 'info')) {
				this.alertTimeout = setTimeout(() => PageUtil.hideAlert(), 5000);
			}
		});

		PageUtil.getRequestObservable().subscribe(
			data => setTimeout(() => {
				if (this.requesting === data) {
					if (this.requesting) {
						this.start();
					}
				} else {

					this.requesting = data;

					if (this.requesting) this.start(); else this.complete();
				}
			}
				, 0));

	}


	alertClosed() {
		PageUtil.hideAlert();
	}

	ngAfterViewInit() {
		
	}


	toggleMenu() {
		this.minified = !this.minified;
		this.srvMasterData.saveDefaultParameter(this.prefix, 'minified', this.minified + '');

		jQuery('.sidebar .nav-item.dropdown').children('ul').collapse('hide');
		jQuery('.sidebar .nav-item.dropdown').children('a').addClass('collapsed');

		if (this.componentRef.relayout) {
			setTimeout(() => {
				this.componentRef.relayout();
			});
		} else {
			// console.log("child component doesn't have relayout method");
		}
		
	}

	onActivate(componentRef){
		this.componentRef = componentRef;
	}

	menuClicked($event: MouseEvent) {
		if (!this.minified) {
			jQuery($event.currentTarget).siblings('ul').collapse('toggle');
			jQuery($event.currentTarget).toggleClass('collapsed');
		}
		$event.preventDefault();
	}

	logout() {
		this.userService.logout();
		// this.router.navigateByUrl('/login'); //if you want to go to login page
		this.router.navigateByUrl('/public');
	}

	start() {
		// Stop current timer
		this.stop();
		// Make it visible for sure
		this.loadingProgress = 15;
		this.show = true;
		// Run the timer with milliseconds iterval
		this._intervalCounterId = setInterval(() => {
			// Increment the progress and update view component
			this.loadingProgress = this.loadingProgress + 3;
			// If the progress is 100% - call complete
			if (this.loadingProgress > 90) this.loadingProgress = 90;
		}, this.interval);
	}

	@HostListener('window:resize', ['$event']) windowResize(event) {
		let w = event.target.innerWidth;
		if (w < 768) {
			this.main_menu_collapsed = false;
		}
	}
	stop() {
		if (this._intervalCounterId) {
			clearInterval(this._intervalCounterId);
			this._intervalCounterId = null;
		}
	}

	complete() {
		this.loadingProgress = 100;
		this.stop();
		setTimeout(() => {
			this.show = false;
			setTimeout(() => {
				// Drop to 0
				this.loadingProgress = 15;
			}, 250);
		}, 250);
	}

	changeLanguage(val: string) {
		localStorage.setItem(Config.appName + '.' + 'lang', val);
		window.location.replace(window.location.href);
	}

	private swipeCoord = [];
	private swipeTime: number;

	swipe(e: TouchEvent, when: string): void {
		const coord: [number, number] = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
		const time = new Date().getTime();
		if (when === 'start') {
			this.swipeCoord = coord;
			this.swipeTime = time;
		} else if (when === 'end') {
			const direction = [coord[0] - this.swipeCoord[0], coord[1] - this.swipeCoord[1]];
			const duration = time - this.swipeTime;
			if (duration < 1000 //
				&& Math.abs(direction[0]) > 30 // Long enough
				&& Math.abs(direction[0]) > Math.abs(direction[1] * 3)) { // Horizontal enough
				const swipe = direction[0] >= 0 ? 'show' : 'hide';
				if (swipe === 'show' && this.swipeCoord[0] < 50) {
					this.mobileMenuShow = true;
				} else if (swipe === 'hide') {
					this.mobileMenuShow = false;
				}
				// Do whatever you want with swipe
			}
		}
	}

}
