import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CmpHome} from './CmpHome';
import {CmpHelp} from './CmpHelp';
import {RoutHome} from './RoutHome';
import {MdlShared} from '../shared/MdlShared';
import { MdlDashboard } from 'app/dashboard/MdlDashboard';
import {MatRippleModule} from '@angular/material/core';


@NgModule({
	imports: [CommonModule, RoutHome, MdlShared, MdlDashboard, MatRippleModule],
	declarations: [CmpHome, CmpHelp],
	exports: [CmpHome],
	providers: []
})
export class MdlHome {}
