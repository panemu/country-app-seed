import {NgModule} from '@angular/core';
import {MdlShared} from '../shared/MdlShared';
import {RoutCity} from './RoutCity';
import {CmpCityList} from './CmpCityList';
import {CmpCityForm} from './CmpCityForm';
import {PrvCity} from './PrvCity';


@NgModule({
	imports: [RoutCity, MdlShared],
	declarations: [CmpCityList, CmpCityForm],
	providers: [PrvCity]
})
export class MdlCity {}