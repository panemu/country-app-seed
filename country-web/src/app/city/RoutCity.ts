import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CmpCityList} from './CmpCityList';
import {CmpCityForm} from './CmpCityForm';

@NgModule({
	imports: [
		RouterModule.forChild([
			{path: ':id', component: CmpCityForm},
			{path: '', component: CmpCityList},
		])
	],
})
export class RoutCity {}