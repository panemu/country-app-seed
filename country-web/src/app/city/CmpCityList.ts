import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TableQuery, TableCriteria } from '../shared/TabelQuery';
import { BaseListComponent } from '../shared/BaseListComponent';
import { PrvCity } from './PrvCity';
import { TranslateService } from '@ngx-translate/core';
import { SrvMasterData } from '../shared/SrvMasterData';
import { SrvUtil } from 'app/shared/SrvUtil';
import { MatDialog } from '@angular/material/dialog';

@Component({
	templateUrl: './CmpCityList.html',
})

export class CmpCityList extends BaseListComponent implements OnInit {

	selected = [];
	tableHeight: string;
	insertMode = false;
	lstContinent = ['Asia', 'Europe', 'Africa', 'Oceania', 'North America', 'Antarctica', 'South America', 'Australia'];
	lstCountry: any;
	canMaintain = false;
	showMenu = false;
	constructor(
		private prvCity: PrvCity,
		router: Router,
		private dialog: MatDialog,
		route: ActivatedRoute,
		translate: TranslateService,
		srvUtil: SrvUtil,
		public srvMasterData: SrvMasterData) {
		super('CmpCityList', translate, route, router, srvUtil);
		this.canMaintain = this.srvMasterData.hasPermission('city:write');

		this.criteria = {
			name: { value: '', label: 'city.name' },
			country: { value: '', label: 'country' },
			continent: { value: '', label: 'continent' },
		};
	}

	ngOnInit() {
		this.tableHeight = 'calc(100vh - 180px)';
		this.defaultOnInit();
		this.getLstCountryByContinent();
	}

	getData(query: TableQuery) {
		return this.prvCity.find(this.paginationObject.startIndex, this.paginationObject.maxRows, query);
	}

	getLstCountryByContinent() {
		this.PageUtil.showRequestIndicator();
		this.prvCity.findCountry(this.criteria.continent.value).subscribe(
			(data: any) => {
				this.lstCountry = data;
				this.PageUtil.hideRequestIndicator();
			}
		);
	}

	edit() {
		if (this.selected && this.selected.length > 0) {
			this.router.navigate(['./' + this.selected[0].id], { relativeTo: this.route });
		}
	}
	deleteSelected() {
		if (this.selected && this.selected.length > 0) {
			this.srvUtil.showDialogConfirm(
				'<strong>' + this.translate.instant('delete.confirmation')).then(
				(answer: string) => {
					if (answer === 'yes') {
						this.PageUtil.showRequestIndicator();
						this.prvCity.deleteRecord(this.selected[0].id).subscribe(data => this.reload(), error => {
							this.srvUtil.handleError(error, this.router);
						});
					}
				});
		}
	}

	exportData() {
		let query = this.getQueryObject();
		this.prvCity.exportData(this.paginationObject.startIndex, this.paginationObject.maxRows, query).subscribe((res) => {
			window['saveAs'](res, 'city.xlsx');
		}, error => {
			this.srvUtil.handleError(error, this.router);
		});
	}

	showSearch(pnlSearch) {
		this.dialog.open(pnlSearch);
	}
}