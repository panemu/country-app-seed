import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '../shared/base.service';
import {TableQuery} from '../shared/TabelQuery';

@Injectable()
export class PrvCity extends BaseService {
	constructor(private http: HttpClient) {
		super();
	}
	find(start: number, max: number, tq: TableQuery) {
		return this.http.post(this.apiUrl + 'city?start=' + start + '&max=' + max, JSON.stringify(tq));
	}
	findById(id: number) {
		return this.http.get(this.apiUrl + 'city/' + id);
	}
	findCountry(continent: string) {
		if (continent == null || continent == undefined) {
			continent = '';
		}
		return this.http.get(this.apiUrl + 'city/countryList?continent=' + continent);
	}
	save(id: number, data: any) {
		return this.http.put(this.apiUrl + 'city/' + id, JSON.stringify(data));
	}
	deleteRecord(id: number) {
		return this.http.delete(this.apiUrl + 'city/' + id);
	}
	exportData(start: number, max: number, tq: TableQuery) {
		return this.http.post(this.apiUrl + 'city/xls?start=' + start + '&max=' + max, JSON.stringify(tq), {
			responseType: 'blob'
		});
	}
	errorNoParam() {
		return this.http.get(this.apiUrl + 'city/errornoparam');
	}
	errorWithParam() {
		return this.http.get(this.apiUrl + 'city/errorwithparam');
	}
	unexpectedError() {
		return this.http.get(this.apiUrl + 'city/unhandled_error');
	}

	unauthorizedError() {
		return this.http.get(this.apiUrl + 'city/unauthorized_error');
	}
}