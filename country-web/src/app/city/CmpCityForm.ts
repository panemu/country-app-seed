import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../shared/base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { PrvCity } from './PrvCity';
import { SrvUtil } from 'app/shared/SrvUtil';

class Model {
	id: number;
	name: string;
	countryId: number;
	countryName: string;
	continent: string;
}

interface ValidationItem {key: string; param?: any}
class FormError {
	name: ValidationItem = null;
	countryId: ValidationItem = null;
}

@Component({
	templateUrl: './CmpCityForm.html',
})

export class CmpCityForm extends BaseComponent implements OnInit {

	id: number;
	model: Model;
	myForm: NgForm;
	@ViewChild('myForm') currentForm: NgForm;
	saving = false;
	lstCountry: any[];
	lstContinent = ['Asia', 'Europe', 'Africa', 'Oceania', 'North America', 'Antarctica', 'South America', 'Australia'];
	sorts: any[];
	continent: string;
	alertDangerClicked = false;

	constructor(private route: ActivatedRoute,
		private prvCity: PrvCity,
		private router: Router,
		private srvUtil: SrvUtil,
		private translate: TranslateService) {
		super();
	}
	ngOnInit() {
		this.id = +this.route.snapshot.params['id'];
		if (this.id !== 0) {
			this.PageUtil.showRequestIndicator();
			this.getData();
		} else {
			this.model = new Model;
		}
	}

	getData() {
		this.prvCity.findById(this.id).subscribe(
			(data: any) => {
				this.model = data;
				this.continent = data.continent;
				this.getLstCountry();
				this.PageUtil.hideRequestIndicator();
			},
			(error) => this.srvUtil.handleError(error, this.router)
		);
	}

	getLstCountry() {
		this.PageUtil.showRequestIndicator();
		this.prvCity.findCountry(this.continent).subscribe(
			(data: any) => {
				this.lstCountry = data;
				this.PageUtil.hideRequestIndicator();
			}
		);
	}

	onSubmit() {
		if (this.saving) return;
		this.PageUtil.showRequestIndicator();
		this.saving = true;
		this.prvCity.save(this.id, this.model).subscribe(
			(data: any) => {
				this.PageUtil.hideRequestIndicator();
				this.saving = false;
				this.PageUtil.showAlertSuccess(this.translate.instant('data.saved'));
				if (this.id === 0) {
					this.id = +data;
					this.router.navigate(['../' + this.id], { relativeTo: this.route });
				}
				this.getData();
			},
			error => {
				this.srvUtil.handleError(error, this.router);
				this.saving = false;
			}
		)
	}

	ngAfterViewChecked() {
		this.formChanged();
	}

	formChanged() {
		if (this.currentForm === this.myForm) { return; }
		this.myForm = this.currentForm;
		if (this.myForm) {
			this.myForm.valueChanges
				.subscribe(data => this.onValueChanged(data));
		}
	}

	onValueChanged(data?: any) {
		if (!this.myForm) { return; }
		const form = this.myForm.form;

		for (const field of Object.keys(this.formErrors)) {
			this.formErrors[field] = '';
			const control = form.get(field);

			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key of Object.keys(control.errors)) {
					this.formErrors[field] = messages[key];
				}
			}
		}
	}
	formErrors = new FormError();

	validationMessages = {
		name: {
			required: { key: 'this.field.is.required' },
			maxlength: { key: 'max.char', param: { par1: '255' } },
			minlength: { key: 'min.char', param: { par1: '4' } },
		},
		countryId: {
			required: { key: 'this.field.is.required' },
		},
	};
}
