import { Component, HostListener, OnInit } from '@angular/core';


@Component({
	selector: 'cmp-public',
	styleUrls: ['CmpPublic.scss'],
	templateUrl: 'CmpPublic.html'
})

export class CmpPublic implements OnInit {
	scrolled: boolean = false;
	dropMenu = false;
	constructor() {
		this.scrolled = window.pageYOffset > 48;
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
	 this.scrolled = window.pageYOffset > 48;
	 console.log(`scrolled ${this.scrolled}`);
  }
	ngOnInit() { }
}