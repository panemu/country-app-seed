import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CmpLandingPage } from './CmpLandingPage';
import { CmpPublic } from './CmpPublic';
import { CmpAbout } from './CmpAbout';
import { RoutPublic } from './RoutPublic';
import { CommonModule } from '@angular/common';


@NgModule({
	imports: [RouterModule, RoutPublic, CommonModule],
	exports: [],
	declarations: [CmpPublic, CmpLandingPage, CmpAbout],
	providers: [],
})
export class MdlPublic { }
