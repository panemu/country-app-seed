import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

/* App Root */
import { AppComponent } from './app.component';

/* Routing Module */
import { AppRoutingModule } from './app-routing.module';
import { MdlHome } from './home/MdlHome';
import { MdlDashboard } from './dashboard/MdlDashboard';
import { MdlShared } from './shared/MdlShared';
import { PageNotFoundComponent } from './not-found.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdlPublic } from './public/MdlPublic';
import { MdlAuth } from './login/MdlAuth';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	imports: [TranslateModule.forRoot({
		loader: {
			provide: TranslateLoader,
			useFactory: HttpLoaderFactory,
			deps: [HttpClient]
		}
	}), BrowserModule, AppRoutingModule, BrowserAnimationsModule,
		HttpClientModule, AppRoutingModule, MdlAuth, MdlPublic, MdlShared.forRoot(), ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
	],
	declarations: [AppComponent, PageNotFoundComponent],
	bootstrap: [AppComponent]
})
export class AppModule {
	constructor() {
		console.log('href: ' + location.href);
	}
}
