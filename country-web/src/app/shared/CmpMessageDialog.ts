import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageDialogObject } from './MessageDialogObject';

@Component({
	selector: 'cmp-message-dialog',
	templateUrl: './CmpMessageDialog.html',
	styleUrls: ['./CmpMessageDialog.scss']
})

export class CmpMessageDialog {
	titleClass = '';
	titleIcon = '';
	constructor(public dialogRef: MatDialogRef<CmpMessageDialog>,
		@Inject(MAT_DIALOG_DATA) public data: MessageDialogObject) {
		if (data.type === 'info') {
			this.titleClass = 'mat-dialog-title bg-blue-700 text-white';
			this.titleIcon = 'icon-info-circled';
		} else if (data.type === 'confirm') {
			this.titleClass = 'mat-dialog-title bg-yellow-500 text-yellow-900';
			this.titleIcon = 'icon-attention';
		} else if (data.type === 'error') {
			this.titleClass = 'mat-dialog-title bg-red-700 text-white';
			this.titleIcon = 'icon-block';
		}
		if (data.noLabel || data.cancelLabel) {
			this.dialogRef.disableClose = true;
		}
	}
	close(answer) {
		this.dialogRef.close(answer);
	}
}