import { Component, OnInit, Inject } from '@angular/core';
import { ProgressInfo } from 'app/shared/PrvAsset';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'cmp-file-upload',
	templateUrl: 'CmpFileUpload.html'
})

export class CmpFileUpload implements OnInit {
	constructor(
		public dialogRef: MatDialogRef<CmpFileUpload>,
		@Inject(MAT_DIALOG_DATA) public progressInfo: ProgressInfo) { }

	ngOnInit() { }
}