import { Component, Input, AfterViewInit, ElementRef, Output, EventEmitter, ViewChild } from '@angular/core';

export type AlertType = 'success' | 'info' | 'warning' | 'danger';

declare var jQuery: any;

export class Alert {
	message: string;
	type: AlertType;

	constructor(message: string, mtype: AlertType) {
		this.message = message;
		this.type = mtype;
	}
}

@Component({
	selector: 'b-alert',
	template: `
<div #bAlert [class]="'alert alert-'+message.type" role="alert" (click)="close()">
	 <span class="close cursor-pointer flex items-start justify-between w-full p-4 rounded shadow-lg" title="close">
	 {{message.message}} &nbsp;
	 
   <svg class="fill-current" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
      </svg>
    </span>
  </div>`,
	styleUrls: ['./CmpAlert.scss']
})

/**
 * Bootstrap alert component. Possible value of type is success, info, warning and danger
 */
export class AlertComponent {
	@ViewChild('bAlert', { static: true }) bAlert: any;
	@Input() message: Alert;
	@Output() closed: EventEmitter<any> = new EventEmitter();
	constructor() { }

	close() {
		this.closed.emit('closed');
	}
}