import { Component, OnInit, Input } from '@angular/core';
import { AssetManager } from './AssetManager';
import { SrvUtil } from './SrvUtil';
import { AssetToUpload } from './AssetToUpload';
import { Config } from './config/env.config';

@Component({
	selector: 'cmp-asset-manager',
	templateUrl: 'CmpAssetManager.html'
})

export class CmpAssetManager implements OnInit {
	apiUrl = Config.API;
	@Input() assetManager: AssetManager;
	readonly = false;
	constructor(
		private srvUtil: SrvUtil
	) { }

	ngOnInit() { }

	async markDeleteAsset(asset: any) {
		let answer = await this.srvUtil.showDialogConfirm(
			'Anda yakin menghapus gambar teknis <strong>' + asset.filename + '</strong>?');
		if (answer === 'yes') {
			let idx = this.assetManager.persistedAssets.indexOf(asset);
			if (idx > -1) {
				this.assetManager.assetsToDelete.push(this.assetManager.persistedAssets[idx]);
				this.assetManager.persistedAssets.splice(idx, 1);
			}
		}
	}

	deleteUnsavedAsset(unsavedDoc) {
		let idx = this.assetManager.assetsToUpload.indexOf(unsavedDoc);
		if (idx > -1) {
			this.assetManager.assetsToUpload.splice(idx, 1);
		}
	}

	addNewAssets(files) {

		// for (let i = 0; i < event.target.files.length; i++) {
		// 	if (event.target.files[i].size > 10485760) {
		// 		this.srvUtil.showDialogError('Ukuran file ' + event.target.files[i].name + ' melebihi batasan 10 MB!');
		// 		return;
		// 	}
		// }

		for (let i = 0; i < files.length; i++) {
			let doc = new AssetToUpload;
			doc.overwrite = false;
			doc.category = this.assetManager.category;
			doc.subCategory = this.assetManager.subCategory;
			doc.referenceId = this.assetManager.referenceId;
			doc.file = files[i];
			this.assetManager.assetsToUpload.push(doc);
		}

	}


}