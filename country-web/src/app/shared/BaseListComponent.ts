import { BaseComponent } from './base.component';
import { TranslateService } from '@ngx-translate/core';
import { TableQuery, TableCriteria, SortingInfo } from './TabelQuery';
import { Observable } from 'rxjs';
import { PaginationObject } from '../shared/CmpPagination';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { catchError, switchMap } from 'rxjs/operators';
import { Config } from './config/env.config';
import { SrvUtil } from './SrvUtil';
import {TableData} from './TableData';
import { QueryList, ViewChildren, Directive } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
// import { CmpTableHelper } from './CmpTableHelper';

@Directive()
export abstract class BaseListComponent extends BaseComponent {
	protected sorts: any[];
	criteria: any;
	searchLabel = '';
	tableData: TableData = new TableData;
	paginationObject = new PaginationObject();
	noRowSelected = true;
	protected forceSearch = false;
	@ViewChildren(DatatableComponent) childQuery: QueryList<DatatableComponent>;
	// @ViewChildren(CmpTableHelper) childQuery: QueryList<CmpTableHelper>;

	constructor(
		public prefix: string,
		protected translate: TranslateService,
		protected route: ActivatedRoute,
		protected router: Router,
		protected srvUtil: SrvUtil
	) {
		super();
	}

	getQueryObject() {
		let query = new TableQuery;

		if (this.sorts && this.sorts.length > 0) {
			this.sorts.forEach(el => {
				query.sortingInfos.push(new SortingInfo(el.prop, el.dir));
			});
		}
		this.searchLabel = '';
		for (let k in this.criteria) {
			if (this.criteria[k].value) {
				query.tableCriteria.push(new TableCriteria(k, this.criteria[k].value));
				this.searchLabel = this.searchLabel + ' [' + this.translate.instant(this.criteria[k].label) + ' : ' + this.criteria[k].value + ']';
			}
		}
		return query;
	}

	onSort($event) {
		this.sorts = $event;
		this.getDataAbstract().subscribe(data => this.displayData(data));
	}

	getDataAbstract(): Observable<any> {
		let query = this.getQueryObject();
		this.PageUtil.showRequestIndicator();
		return this.getData(query);

	}

	textChange(e) {
		if (e.keyCode == 13) {
			this.reloadFirstPage();
		}
	}

	abstract getData(query: TableQuery): Observable<any>;

	displayData(data: any) {
		this.tableData = data;
		this.paginationObject.totalRows = this.tableData.totalRows;
		this.PageUtil.hideRequestIndicator();
		this.collapsePanel('#pnlBody');
	}

	defaultOnInit() {
		this.sorts = JSON.parse(localStorage.getItem(Config.appName + '.' + this.prefix + '_tbl_sorts'));;

		this.route.params.pipe(switchMap((params: Params) => {
			let load = false;
			this.noRowSelected = true;
			for (const k of Object.keys(this.criteria)) {
				this.criteria[k].value = this.emptyStringIfUndefined(params[k]);
				load = true;
			}

			this.paginationObject.startIndex = this.zeroIfUndefined(params['start']);
			if (load || this.forceSearch) {
				this.PageUtil.showRequestIndicator();
				return this.getDataAbstract().pipe(catchError(err => this.srvUtil.handleErrorReturnObservable(err, this.router)));
			} else {
				return Observable.create(observer => null);
			}
		})).subscribe(data => this.displayData(data));
	}

	reloadFirstPage() {
		this.paginationObject.startIndex = 0;
		this.reload();
	}

	reload() {
		this.forceSearch = true;
		let navObject = { start: this.paginationObject.startIndex, random: Math.random() };
		for (var k in this.criteria) {
			if (this.criteria[k].value) {
				navObject[k] = this.criteria[k].value;
			}
		}
		this.router.navigate(['./', navObject], { relativeTo: this.route });
	}

	onSelect({ selected }) {
		this.noRowSelected = false;
	}

	relayout() {
		this.childQuery.forEach(item => {
			item.rows = [...item.rows];
			// item.repaint();
		});
	}

}