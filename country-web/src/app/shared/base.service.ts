
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Config } from './config/env.config';

export class BaseService {
	apiUrl = Config.API;

	protected handleError(error: Response | any) {
		return observableThrowError(error || 'Server error');
	}

	getApiUrl() {
		return this.apiUrl;
	}
}