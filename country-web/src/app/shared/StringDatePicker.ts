import { Component, ElementRef, forwardRef, OnInit } from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

export const STRINGDATEPICKER_VALUE_ACCESSOR: any = {
	provide: NG_VALUE_ACCESSOR,
	useExisting: forwardRef(() => StringDatePicker),
	multi: true
};

@Component({
	selector: 'string-date-picker',
	template: `
	<input class="form-input" [matDatepicker]="picker" placeholder="Choose a date" [(value)]="_innerValue" (dateChange)="_innerValueChange($event)" [disabled]="_disabled">
	<mat-datepicker-toggle matSuffix [for]="picker" ></mat-datepicker-toggle>
	<mat-datepicker #picker></mat-datepicker>
	`,
	providers: [STRINGDATEPICKER_VALUE_ACCESSOR],
	styleUrls: ['StringDatePicker.scss']
})

export class StringDatePicker implements OnInit, ControlValueAccessor {
	value: string;
	_innerValue: Date;
	_disabled = false;
	// -----

	constructor(private elRef: ElementRef) {
	}

	ngOnInit() {
		// const inputField = this.elRef.nativeElement.children[0];
		// inputField.className = this.elRef.nativeElement.className;
		// this.elRef.nativeElement.className = '';
	}

	writeValue(obj: any): void {
		// console.log('write value ' + obj);
		if (obj) {
			this._innerValue = new Date(obj);
			this.value = this.toIsoDate(this._innerValue);
		} else {
			this.value = '';
			this._innerValue = null;
		}
		// console.log('== write value ' + this._innerValue);
		this.onChange(this.value);
	}
	setDisabledState?(isDisabled: boolean): void {
		this._disabled = isDisabled;
	}

	private toIsoDate(d: Date): string {
		const dd = this._2digit(d.getDate());
		const mm = this._2digit(d.getMonth() + 1);
		const yy = d.getFullYear();
		const stringDate = yy + '-' + mm + '-' + dd;
		return stringDate;
	}

	_innerValueChange(evt: MatDatepickerInputEvent<Date>) {
		const newVal = evt.value;
		if (newVal) {
			this.value = this.toIsoDate(newVal);
		} else {
			this.value = '';
		}
		// console.log('_innerValueChange ' + this.value + ' - ' + newVal);
		this.onChange(this.value);
	}

	private _2digit(n: number) {
		return ('00' + n).slice(-2);
	}

	onChange = (_: any) => { };
	onTouched = () => { };
	registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
	registerOnTouched(fn: () => void): void { this.onTouched = fn; }

}
