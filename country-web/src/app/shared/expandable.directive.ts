import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
	selector: '[appExpandable]'
})
export class ExpandableDirective {
	private tm;
	private _enabled = true;
	constructor(private el: ElementRef) {
	}

	@Input('appExpandable') set expandable(value: boolean) {
		if (typeof value != "boolean") {
			return;
		}
		this._enabled = value;
		if (!value) {
			let div = this.getElement();
			if (div) {
				this.collapseSection(div);
			}
		}
	}

	collapseSection(element) {
		if (this.tm) clearTimeout(this.tm);
		const dur = +getComputedStyle(element).transitionDuration.replace('s', '');
		// requestAnimationFrame(function () {
			element.style.height = 0 + 'px';
		// });
		element.setAttribute('data-collapsed', 'true');
		this.el.nativeElement.classList.remove('expanded');
		// console.log('dur ' + dur);
		this.tm = setTimeout(() => {
			element.style.height = null;
		}, dur * 1000);
	}

	expandSection(element) {
		// const dur = +getComputedStyle(element).transitionDuration.replace('s','') + 1;
		let sectionHeight = element.scrollHeight;
		element.style.height = sectionHeight + 'px';
		element.setAttribute('data-collapsed', 'false');
		element.setAttribute('data-collapsed', 'false');
		this.el.nativeElement.classList.add('expanded');
	}

	private getElement() {
		let trigger:HTMLElement = this.el.nativeElement;
		let div = trigger.nextElementSibling;
		return div;
	}

	@HostListener('click', ['$event'])
	onClick($event) {
		if (!this._enabled) { return; }
		let div = this.getElement();
		var isCollapsed = div.getAttribute('data-collapsed') !== 'false';
		if (isCollapsed) {
			this.expandSection(div);
		} else {
			this.collapseSection(div);
		}
		$event.stopPropagation();
	}

}
