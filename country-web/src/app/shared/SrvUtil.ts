import { Injectable } from '@angular/core';
import { MessageDialogObject } from './MessageDialogObject';
import { MatDialog } from '@angular/material/dialog';
import { CmpMessageDialog } from './CmpMessageDialog';
import { Observable, Subject, Subscriber } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Config } from './config/env.config';
import { PageUtil } from './PageUtil';

@Injectable()
export class SrvUtil {
	private requestObserver: any;
	private reloadSubject = new Subject<Date>();
	private requestObservable = new Observable<boolean>(observer => {
		this.requestObserver = observer;
	});

	constructor(public dialog: MatDialog, public translate: TranslateService) { }

	getRequestObservable(): Observable<boolean> {
		return this.requestObservable;
	}

	showRequestIndicator() {
		this.requestObserver.next(true);
	}

	hideRequestIndicator() {
		this.requestObserver.next(false);
	}

	getReloadAccountEvent() {
		return this.reloadSubject;
	}

	reloadAccount() {
		this.reloadSubject.next(new Date);
	}

	public showDialogInfo(htmlContent: string): Promise<'yes' | 'no' | 'cancel'> {
		return this.showDialog({
			content: htmlContent,
			type: 'info',
			yesLabel: 'Ok',
		});
	}

	public showDialogError(htmlContent: string): Promise<'yes' | 'no' | 'cancel'> {
		return this.showDialog({
			content: htmlContent,
			type: 'error',
			yesLabel: 'Ok',
		});
	}

	public showDialogConfirm(htmlContent: string): Promise<'yes' | 'no' | 'cancel'> {
		return this.showDialog({
			content: htmlContent,
			type: 'confirm',
			yesLabel: 'Yes',
			noLabel: 'No'
		});
	}

	public showDialog(o: MessageDialogObject): Promise<'yes' | 'no' | 'cancel'> {
		if (!o.yesLabel) { o.yesLabel = 'Ok'; }
		if (!o.type) { o.type = 'info'; }
		if (!o.title) {
			if (o.type === 'info') {
				o.title = 'Info';
			} else if (o.type === 'error') {
				o.title = 'Error';
			} else if (o.type === 'confirm') {
				o.title = 'Confirm';
			}
		}

		const dialogRef = this.dialog.open(CmpMessageDialog, {
			data: o,
			position: { top: '50px' }
		});

		return dialogRef.afterClosed().toPromise<'yes' | 'no' | 'cancel'>();
	}

	handleErrorReturnObservable(error, router: Router) {
		this.handleError(error, router);
		return Observable.create(observer => null);
	}

	handleError(error, router: Router) {
		console.error(`Backend returned error ${JSON.stringify(error)}`);
		PageUtil.hideRequestIndicator();
		let errorMessage = '';
		// let title = error.status;
		let body = null;
		if (error.error) {
			//this is error from http	
			body = error.error;
			if (body instanceof Blob) {
				let reader = new FileReader();
				reader.addEventListener('loadend', (e) => {
					error.error = JSON.parse(reader.result + '');
					this.handleError(error, router);
				});
				reader.readAsText(body);
				return;
			} else {
				errorMessage = error.error;
			}
		} else if (error.body) {
			//this is error from ionic filetransfer	
			body = JSON.parse(error.body);
		}


		if (body && body.code) {

			let paramObject = {};
			if (body.parameters) {
				let params: any[] = body.parameters;
				params.forEach((val, idx) => paramObject['par' + idx] = val);
			}

			errorMessage = this.translate.instant(body.code, paramObject);
			if (errorMessage == body.code) {
				errorMessage = body.message;
			}
		}
		if (error.status === 401) {
			this.toLoginPage(router);
			return;
		} else if (error.status === 500) {
			let now = new Date();
			errorMessage = 'There was an unknown error while processing your request. ' + errorMessage
				+ '<br><br><small>Time: ' + now + '</small>';

			if (error.url) {
				errorMessage = errorMessage + '<br><small>URL: ' + error.url + '</small>';
			}
			errorMessage = errorMessage + '<br><br>Please contact our support.';


		}
		// PanictUtil.showAlertDanger(subTitle);
		this.showDialogError(errorMessage);
	}

	toLoginPage(router: Router, withOrigin: boolean = true) {
		localStorage.removeItem(Config.appName + '.' + 'auth_info');
		if (withOrigin) {
			router.navigate(['/login', { origin: encodeURIComponent(router.url) }]);
		} else {
			router.navigate(['/login', { origin: encodeURIComponent('/home') }]);
		}
	}
}
