import { Injectable, NgZone } from '@angular/core';
import { Observable, concat, merge, from } from 'rxjs';
import { Config } from './config/env.config';
import { AssetToUpload } from './AssetToUpload';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map, filter } from 'rxjs/operators';
import { PersistedAsset } from './AssetManager';
export class ProgressInfo {
	message: string;
	progress: number;
	_loaded = 0;
	_totalLoaded = 0;
}

@Injectable()
export class PrvAsset {

	constructor(private http: HttpClient) { }

	uploadAssets(assets: AssetToUpload[], progressInfo?: ProgressInfo): Observable<any> {
		console.log('   srv uploadAssets');
		let obs = [];
		let totalSize = 0;
		if (progressInfo) {
			progressInfo._loaded = 0;
			progressInfo._totalLoaded = 0;
		}
		assets.forEach(item => totalSize = totalSize + item.file.size);
		assets.forEach(item => obs.push(this.uploadAsset(item, progressInfo, totalSize)));
		let obsConcat = concat(...obs);
		return obsConcat;
	}

	uploadAsset(asset: AssetToUpload, progressInfo?: ProgressInfo, totalSize = 0) {
		let form = new FormData();
		form.append('attachment', asset.file, asset.file.name);
		if (asset.category) {
			form.append('category', asset.category);
		}
		if (asset.referenceId) {
			form.append('refId', asset.referenceId + '');
		}
		if (asset.description) {
			form.append('description', asset.description);
		}
		if (asset.subCategory) {
			form.append('subCategory', asset.subCategory);
		}
		if (asset.overwrite) {
			form.append('overwrite', asset.overwrite + '');
		} else {
			form.append('overwrite', 'false');
		}
		if (!totalSize) {
			totalSize = asset.file.size;
		}
		if (!progressInfo) {
			progressInfo = new ProgressInfo;
		}

		return this.http.post<any>(Config.API + 'asset/upload', form, {
			reportProgress: true,
			observe: 'events'
		}).pipe(
			map(event => {
				if (event.type === HttpEventType.UploadProgress) {
					const percentDone = Math.round(100 * (event.loaded + progressInfo._totalLoaded) / totalSize);
					progressInfo.message = 'Uploading ' + asset.file.name;
					progressInfo.progress = percentDone;
					progressInfo._loaded = event.loaded;
				} else if (event.type === HttpEventType.Response) {
					progressInfo._totalLoaded = progressInfo._totalLoaded + progressInfo._loaded;
					return event.body;
				}
			}),
			filter(data => data !== undefined)
			// catchError(this.handleError)
		);
	}


	deleteAssets(...assets: PersistedAsset[]) {
		console.log('   srv deleteAssets');
		let ids = [];
		assets.forEach(item => {
			ids.push(item.id);
		});
		let stringIds = ids.join(',');
		return this.http.delete(Config.API + 'asset/' + stringIds);
	}

	saveAssets(...assets: PersistedAsset[]) {
		console.log('   srv saveAsset');
		return this.http.put(Config.API + 'asset',  JSON.stringify(assets));
	}

}