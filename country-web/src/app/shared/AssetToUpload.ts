export class AssetToUpload {
	file: any;
	category: string;
	description: string;
	subCategory: string;
	overwrite = false;
	referenceId = 0;
}
