import { AssetToUpload } from './AssetToUpload';

export class PersistedAsset {
	id: number;
	category: string;
	refId: number;
	path: string;
	pathSmall: string;
	description: string;
	size: number;
	filename: string;
}

export class AssetManager {
	persistedAssets: PersistedAsset[] = [];
	assetsToUpload: AssetToUpload[] = [];
	assetsToDelete: PersistedAsset[] = [];
	category: string;
	subCategory = '';
	private _referenceId: number;

	set referenceId(refId: number) {
		this._referenceId = refId;
		this.assetsToUpload.forEach(item => item.referenceId = this._referenceId);
	}

	get referenceId() {
		return this._referenceId;
	}

	clear() {
		this.persistedAssets = [];
		this.assetsToUpload = [];
		this.assetsToDelete = [];
	}
}