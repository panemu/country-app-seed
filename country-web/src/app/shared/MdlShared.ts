import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CmpDynabox } from './CmpDynabox';
import { CmpTypeahead } from './CmpTypeahead';
import { AlertComponent } from './CmpAlert';
import { UserService } from './user.service';
import { SrvMasterData } from './SrvMasterData';
import { StringDatePicker} from './StringDatePicker';
import { DefaultHttpInterceptor } from './DefaultHttpInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CmpTableHelper, CmpTableHelperSaveDialog } from './CmpTableHelper';
import { CmpPagination } from './CmpPagination';
import { TranslateModule } from '@ngx-translate/core';
import { UppercaseDirective } from './UppercaseDirective';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { SrvUtil } from './SrvUtil';
import { CmpMessageDialog } from './CmpMessageDialog';
import { PrvAsset } from './PrvAsset';
import { CmpAssetManager } from './CmpAssetManager';
import { FileSizePipe } from './FileSizePipe';
import { CmpFileUpload } from './CmpFileUpload';
import { DragDropDirective } from './DragDropDirective';
import { ExpandableDirective } from './expandable.directive';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
	imports: [NgxDatatableModule,
		CommonModule,
		RouterModule,
		FormsModule,
		TranslateModule,
		MatDialogModule,
		MatNativeDateModule,
		MatDatepickerModule],
	declarations: [CmpTableHelper, CmpPagination, AlertComponent, CmpDynabox, CmpTypeahead, UppercaseDirective,
		CmpMessageDialog, CmpTableHelperSaveDialog, CmpAssetManager, FileSizePipe, CmpFileUpload, StringDatePicker,
		DragDropDirective, ExpandableDirective],
	exports: [NgxDatatableModule, CmpTableHelper, CmpPagination, CommonModule, FormsModule, RouterModule, AlertComponent,
		CmpDynabox, CmpTypeahead, TranslateModule, UppercaseDirective, MatDialogModule, MatDatepickerModule,
		CmpAssetManager, FileSizePipe, CmpFileUpload, StringDatePicker, ExpandableDirective],
	entryComponents: [CmpMessageDialog, CmpTableHelperSaveDialog, CmpFileUpload]
})
export class MdlShared {

	static forRoot(): ModuleWithProviders<MdlShared> {
		return {
			ngModule: MdlShared,
			providers: [UserService, SrvMasterData, PrvAsset,
				{
					provide: HTTP_INTERCEPTORS,
					useClass: DefaultHttpInterceptor,
					multi: true,
				},
				SrvUtil]
		};
	}
}
