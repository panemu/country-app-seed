import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {

	int _selectedIndex = 0;

	AppDrawer(this._selectedIndex);

  void _onMenuTap(BuildContext context, int index) {
	  Navigator.pop(context);
  	if (index == _selectedIndex) return;

		if (index == 0) {
			Navigator.pop(context);
		} else if (index == 1) {
			if (_selectedIndex == 0) {
				Navigator.pushNamed(context, '/country');
			} else {
				Navigator.pushReplacementNamed(context, '/country');
			}
		} else if (index == 2) {
			if (_selectedIndex == 0) {
				Navigator.pushNamed(context, '/city');
			} else {
				Navigator.pushReplacementNamed(context, '/city');
			}
		}
	}

	@override
	Widget build(BuildContext context) {
		return Drawer(
			child: Column(
				children: [
					ListTile(
						title: Text("Home"),
						onTap: () => _onMenuTap(context, 0),
					),ListTile(
						title: Text("Country"),
						onTap: () => _onMenuTap(context, 1),
					),ListTile(
						title: Text("City"),
						onTap: () => _onMenuTap(context, 2),
					),
				],
			),
		);
	}
}
