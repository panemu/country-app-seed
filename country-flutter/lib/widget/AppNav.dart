import 'package:flutter/material.dart';

class AppNav extends StatelessWidget {
	final int _selectedIndex;

	AppNav(this._selectedIndex);

	_onItemTapped(BuildContext context, int index) {
		if (index == _selectedIndex) return;
		if (index == 0) {
			Navigator.pop(context);
		} else if (index == 1) {
			if (_selectedIndex == 0) {
				Navigator.pushNamed(context, '/country');
			} else {
				Navigator.pushReplacementNamed(context, '/country');
			}
		} else if (index == 2) {
			if (_selectedIndex == 0) {
				Navigator.pushNamed(context, '/city');
			} else {
				Navigator.pushReplacementNamed(context, '/city');
			}
		}
	}

	@override
	Widget build(BuildContext context) {
		return BottomNavigationBar(
			items: const <BottomNavigationBarItem>[
				BottomNavigationBarItem(
					icon: Icon(Icons.home),
					title: Text('Home'),
				),
				BottomNavigationBarItem(
					icon: Icon(Icons.business),
					title: Text('Country'),
				),
				BottomNavigationBarItem(
					icon: Icon(Icons.school),
					title: Text('City'),
				),
			],
			currentIndex: _selectedIndex,
			selectedItemColor: Colors.amber[800],
			onTap: (index) {
				_onItemTapped(context, index);
			},
		);
	}
}
