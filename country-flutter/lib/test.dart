import 'dart:convert';
import 'dart:io';

import 'package:intl/intl.dart';

void main() {
	String tanggal = DateFormat('yyyy-MM-dd').format(DateTime.now());
	print('tangal $tanggal');
	for (int i = 0; i < 5; i++) {
		print('hello ${i + 1}');
	}


	var myFile = new File('env.json');
	print('${myFile.absolute.path}');
	myFile.readAsString()
		.then((fileContents) => jsonDecode(fileContents))
		.then((jsonData) {
		print('jsonData $jsonData');
		// do whatever you want with the data
	});
}
