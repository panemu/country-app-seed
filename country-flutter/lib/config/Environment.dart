import 'package:flutter/material.dart';
import 'package:flutter_app_seed/main.dart';

abstract class Environment {
	static Environment value;
	String apiUrl;
	int delay;
	bool debug;
	bool printResponse;

	Environment() {
		value = this;
		runApp(CountryAppSeed());
	}

	String get name => runtimeType.toString();

	@override
	String toString() {
		return '$name {apiUrl: $apiUrl, delay: $delay, debug: $debug}';
	}


}