import 'package:flutter_app_seed/config/Environment.dart';

void main() => Development();

class Development extends Environment {
//	final apiUrl = 'http://10.0.2.2:8080/country/api2/';
	final apiUrl = 'https://live.panemu.com/country/api2/';
	final delay = 1;
	final debug = true;
	final printResponse = true;
}