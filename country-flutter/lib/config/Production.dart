import 'package:flutter_app_seed/config/Environment.dart';

void main() => Production();

class Production extends Environment {
	final String apiUrl = 'https://scs-demo.panemu.com/country/api2/';
	final int delay = 0;
	final bool debug = false;
	final printResponse = false;
}