import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/BaseApi.dart';
import 'package:flutter_app_seed/config/Development.dart';
import 'package:flutter_app_seed/config/Environment.dart';
import 'package:flutter_app_seed/dto/AuthInfo.dart';
import 'package:flutter_app_seed/localization/AppLocalizations.dart';
import 'package:flutter_app_seed/ui/HomePage.dart';
import 'package:flutter_app_seed/ui/LoginPage.dart';
import 'package:flutter_app_seed/ui/city/UiCityList.dart';
import 'package:flutter_app_seed/ui/country/UiCountryList.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => Development();

class CountryAppSeed extends StatefulWidget {

	@override
	State<StatefulWidget> createState() {
		Environment env = Environment.value;
		print('Environment info $env');
		return CountryAppSeedState();
	}
}

class CountryAppSeedState extends State<CountryAppSeed> {

	Widget firstPage;
	FutureBuilder fb;

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Flutter Demo',
			localizationsDelegates: [
				const AppLocalizationsDelegate(),
				GlobalWidgetsLocalizations.delegate,
				GlobalMaterialLocalizations.delegate
			],
			supportedLocales: [
				const Locale('en', ''),
				const Locale('id', ''),
			],
			theme: ThemeData(
				// This is the theme of your application.
				//
				// Try running your application with "flutter run". You'll see the
				// application has a blue toolbar. Then, without quitting the app, try
				// changing the primarySwatch below to Colors.green and then invoke
				// "hot reload" (press "r" in the console where you ran "flutter run",
				// or simply save your changes to "hot reload" in a Flutter IDE).
				// Notice that the counter didn't reset back to zero; the application
				// is not restarted.
				primarySwatch: Colors.blue,
			),
			home: firstPage == null ? CircularProgressIndicator() : firstPage,
			routes: <String, WidgetBuilder>{
				// Set routes for using the Navigator.
				'/home': (BuildContext context) => HomePage(),
				'/country': (BuildContext context) => UiCountryList(),
				'/login': (BuildContext context) => LoginPage(),
				'/city': (BuildContext context) => UiCityList()
			},
		);
	}

	@override
	void initState() {
		SharedPreferences.getInstance().then((pref) {
			String authInfo = pref.get("auth_info");
			if (authInfo?.isNotEmpty ?? false) {
				print("auth info loaded");
				final JsonDecoder _decoder = JsonDecoder();
				AuthInfo au = AuthInfo(_decoder.convert(authInfo));
				BaseApi.AUTH_TOKEN = au.authToken;
				setState(() => firstPage = HomePage());
			} else {
				setState(() => firstPage = LoginPage());
				print("no auth info");
			}
		});


		super.initState();
	}
}