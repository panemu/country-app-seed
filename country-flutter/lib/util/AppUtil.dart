import 'package:intl/intl.dart';

class AppUtil {
	static const ISO_DATE_FORMAT = 'yyyy-MM-dd';

	static const List<String> CONTINENT_LIST = [
		'',
		'Asia',
		'Europe',
		'Africa',
		'Oceania',
		'North America',
		'Antarctica',
		'South America',
		'Australia'
	];

	static DateTime toDate(String isoDate) {
		DateFormat isoDateFormat = DateFormat(ISO_DATE_FORMAT);
		if (isoDate
			?.trim()
			?.isEmpty ?? true) {
			return null;
		}
		return isoDateFormat.parse(isoDate);
	}

	static String fromDate(DateTime date) {
		DateFormat isoDateFormat = DateFormat(ISO_DATE_FORMAT);
		return date == null ? '' : isoDateFormat.format(date);
	}
}
