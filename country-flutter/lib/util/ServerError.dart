class ServerError {
	final String code;
	int statusCode;
	final String message;
	final List<String> parameters;

	ServerError(this.code, this.message, this.parameters);

	ServerError.fromJson(Map<String, dynamic> json)
		: code = json['code'],
			message = json['message'],
			parameters = json['parameters']?.cast<String>();

	Map<String, dynamic> toJson() =>
		{
			'statusCode': statusCode,
			'code': code,
			'message': message,
			'parameters': parameters,
		};

}