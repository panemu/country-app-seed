import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

String t(BuildContext context, String key, [List params, String defaultTranslation]) {
	return AppLocalizations.of(context).get(key, params, defaultTranslation);
}

class AppLocalizations {
	AppLocalizations(this.locale);

	final Locale locale;

	static AppLocalizations of(BuildContext context) {
		return Localizations.of<AppLocalizations>(context, AppLocalizations);
	}

	static Map<String, String> _localizedValues = {};

	Future<bool> load() async {
		String data = await rootBundle.loadString('assets/i18n/${locale.languageCode}.json');
		Map<String, dynamic> _result = json.decode(data);
		_localizedValues= _result.cast<String, String>();
		return true;
	}

	String get title {
		return _localizedValues['title'];
	}

	String get(String key, [List params, String defaultTranslation]) {
		var text = _localizedValues[key];
		if (text == null || text.trim().isEmpty) {
			return defaultTranslation?.trim()?.isEmpty ?? true ? key : defaultTranslation;
		}

		if (params != null && params.isNotEmpty) {
			for (int i = 0; i < params.length; i++) {
				text = text.replaceAll(r'{{par' + i.toString()+'}}', params[i]);
			}
		}
		return text;
	}
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
	const AppLocalizationsDelegate();

	@override
	bool isSupported(Locale locale) => ['en', 'id'].contains(locale.languageCode);

	@override
	Future<AppLocalizations> load(Locale locale) async {
		var l = AppLocalizations(locale);
		await l.load();
		return l;
	}

	@override
	bool shouldReload(AppLocalizationsDelegate old) => false;
}
