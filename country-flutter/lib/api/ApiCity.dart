import 'package:flutter_app_seed/api/BaseApi.dart';
import 'package:flutter_app_seed/dto/DtoCity.dart';
import 'package:flutter_app_seed/dto/DtoCountryOption.dart';
import 'package:flutter_app_seed/dto/TableData.dart';
import 'package:flutter_app_seed/dto/TableQuery.dart';

class ApiCity {
	final _baseApi = BaseApi();

	Future testErrorWithParam() {
		return _baseApi.get('city/errorwithparam');
	}

	Future testErrorNoParam() {
		return _baseApi.get('city/errornoparam');
	}

	Future testUnexpectedError() {
		return _baseApi.get('city/unhandled_error');
	}

	Future testUnauthorizedError() {
		return _baseApi.get('city/unauthorized_error');
	}

	Future<TableData<DtoCity>> fetch(
		{int start = 0, int max = 10, TableQuery tableQuery}) async {
		final map = await _baseApi.post('city?start=$start&max=$max', tableQuery);
		return TableData.fromMap(
			map, (data) => data.map((aRow) => DtoCity.fromJson(aRow)).toList());
	}

	Future<void> save(DtoCity city) {
		return _baseApi.put('city/${city.id}', city);
	}

	Future<List<DtoCountryOption>> findCountriesByContinent(
		[continent = '']) async {
		final map = await _baseApi.get('city/countryList?continent=$continent');
		List<dynamic> lst = map['list'];
		return lst.map((item) => DtoCountryOption.fromJson(item)).toList();
	}

	Future<void> delete(int id) {
		return _baseApi.delete('city/$id');
	}
}
