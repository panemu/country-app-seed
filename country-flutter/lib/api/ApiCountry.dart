import 'package:flutter_app_seed/api/BaseApi.dart';
import 'package:flutter_app_seed/dto/DtoCountry.dart';
import 'package:flutter_app_seed/dto/TableData.dart';
import 'package:flutter_app_seed/dto/TableQuery.dart';

class ApiCountry {
	final _baseApi = BaseApi();

	Future<TableData<DtoCountry>> fetch(
		{int start = 0, int max = 10, TableQuery tableQuery}) async {
		final map = await _baseApi.post('country?start=$start&max=$max', tableQuery);
		return TableData.fromMap(
			map, (data) =>
			data.map((aRow) => DtoCountry.fromJson(aRow)).toList());
	}

	Future<void> save(DtoCountry dto) {
		return _baseApi.put('country/${dto.id}', dto);
	}

	Future<DtoCountry> findById(int id) async {
		final map = await _baseApi.get('country/$id');
		return DtoCountry.fromJson(map);
	}
}
