import 'dart:convert';
import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:flutter_app_seed/config/Environment.dart';
import 'package:flutter_app_seed/util/ServerError.dart';
import 'package:http/http.dart' as http;

class BaseApi {

	static String AUTH_TOKEN = 'c7chrcjgjqo69egoulisjhi01m';
	final JsonDecoder _decoder = JsonDecoder();

	@protected
	Future<Map<String, dynamic>> post(String pathUrl, [Object payload]) async {
		final body = payload == null ? '' : json.encode(payload);
		if (Environment.value.debug) {
			print('$runtimeType POST $pathUrl $body');
			if (Environment.value.delay > 0) {
				await Future.delayed(Duration(seconds: Environment.value.delay));
			}
		}

		final response = await http.post(
			Environment.value.apiUrl + pathUrl, body: body, headers: {
			HttpHeaders.authorizationHeader: AUTH_TOKEN,
			HttpHeaders.contentTypeHeader: "application/json"
		});

		return _handleResponse(response);
	}

	@protected
	Future<Map<String, dynamic>> put(String pathUrl, [Object payload]) async {
		final body = payload == null ? '' : json.encode(payload);

		if (Environment.value.debug) {
			print('$runtimeType PUT $pathUrl $body');
			if (Environment.value.delay > 0) {
				await Future.delayed(Duration(seconds: Environment.value.delay));
			}
		}

		final response = await http.put(
			Environment.value.apiUrl + pathUrl, body: body, headers: {
			HttpHeaders.authorizationHeader: AUTH_TOKEN,
			HttpHeaders.contentTypeHeader: "application/json"
		});

		return _handleResponse(response);
	}

	@protected
	Future<Map<String, dynamic>> get(String pathUrl) async {
		if (Environment.value.debug) {
			print('$runtimeType GET $pathUrl ');
			if (Environment.value.delay > 0) {
				await Future.delayed(Duration(seconds: Environment.value.delay));
			}
		}
		final response = await http.get(
			Environment.value.apiUrl + pathUrl, headers: {
			HttpHeaders.authorizationHeader: AUTH_TOKEN,
			HttpHeaders.contentTypeHeader: "application/json"
		});
		return _handleResponse(response);
	}

	@protected
	Future<Map<String, dynamic>> delete(String pathUrl) async {
		if (Environment.value.debug) {
			print('$runtimeType DELETE $pathUrl ');
			if (Environment.value.delay > 0) {
				await Future.delayed(Duration(seconds: Environment.value.delay));
			}
		}
		final response = await http.delete(
			Environment.value.apiUrl + pathUrl, headers: {
			HttpHeaders.authorizationHeader: AUTH_TOKEN,
			HttpHeaders.contentTypeHeader: "application/json"
		});
		return _handleResponse(response);
	}

	Map<String, dynamic> _handleResponse(http.Response response) {
		final statusCode = response.statusCode;

		if (statusCode < 200 || statusCode >= 300) {
			_handleError(response);
		} else if (statusCode == 204) {
			return null;
		}
		final jsonBody = Utf8Decoder().convert(response.bodyBytes);
		if (Environment.value.printResponse) {
			print('$runtimeType jsonBody $jsonBody');
		}
		final map = _decoder.convert(jsonBody);
		if (map is List) {
			return {"list": map};
		}
		return map;
	}

	void _handleError(http.Response response) {

		final jsonBody = response.body?.trim();
		_buildErrorObject(jsonBody, response.statusCode, response.reasonPhrase);
	}

	Future<void> _handleStreamedError(http.StreamedResponse response) async {
		final jsonBody = await response.stream.bytesToString();
		_buildErrorObject(jsonBody, response.statusCode, response.reasonPhrase);
	}

	void _buildErrorObject(String jsonBody, int statusCode, String reasonPhrase) {
		if (jsonBody?.isNotEmpty ?? false) {
			Map<String, dynamic> errorInfo;
			try {
				errorInfo = _decoder.convert(jsonBody);
			} catch (err) {
				final now = new DateTime.now().toUtc();
				throw Exception('${statusCode} $jsonBody ($now UTC)');
			}
			if (errorInfo['code'] != null) {
				final se = ServerError.fromJson(errorInfo);
				se.statusCode = statusCode;
				throw se;
			} else {
				final now = new DateTime.now().toUtc();
				throw Exception('$statusCode $jsonBody ($now UTC)');
			}
		}
		throw Exception('$statusCode $reasonPhrase');
	}

	Future<Map<String, dynamic>> upload(String pathUrl, Object payload, String payloadName, File file, {String httpMethod = 'POST'}) async {
		String body;
		if (payload == null) {
			body = '';
		} else if (payload is String) {
			body = payload;
		} else {
			body = json.encode(payload);
		}
		if (Environment.value.debug) {
			print('$runtimeType UPLOAD $pathUrl $body');
			if (Environment.value.delay > 0) {
				await Future.delayed(Duration(seconds: Environment.value.delay));
			}
		}
		var uri = Uri.parse(Environment.value.apiUrl + pathUrl);
		var request = http.MultipartRequest(httpMethod, uri);
		request.headers[HttpHeaders.authorizationHeader] = AUTH_TOKEN;
		request.fields[payloadName] = body;

		if (file != null) {
			request.files.add(await http.MultipartFile.fromPath('attachment', file.path));
		}
		http.StreamedResponse response = await request.send();

		final statusCode = response.statusCode;

		if (statusCode < 200 || statusCode >= 300) {
			await _handleStreamedError(response);
		} else if (statusCode == 204) {
			return null;
		}
		final jsonBody = await response.stream.bytesToString();
		if (Environment.value.printResponse) {
			print('$runtimeType jsonBody $jsonBody');
		}
		final map = _decoder.convert(jsonBody);
		if (map is List) {
			return {"list": map};
		}
		return map;
	}
}
