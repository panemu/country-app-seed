import 'dart:convert';
import 'dart:io';

import 'package:flutter_app_seed/config/Environment.dart';
import 'package:http/http.dart' as http;

class ApiUser {

	Future<String> login(String username, String password) async {
		final dto = {'username': username, 'password': password};
		final stringPayload = jsonEncode(dto);
		final response = await http.post(
			Environment.value.apiUrl + 'auth/authenticate',
			body: stringPayload,
			headers: {HttpHeaders.contentTypeHeader: "application/json"});
		final jsonBody = response.body;
		final statusCode = response.statusCode;

		if (statusCode == 401) {
			throw Exception('Invalid username or password');
		}

		if (statusCode < 200 || statusCode >= 300 || jsonBody == null) {
			throw Exception(
				"Error while getting contacts [StatusCode:$statusCode, Error:${response
					.reasonPhrase}, Message: $jsonBody]");
		}

		return jsonBody;
	}
}
