import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/ApiCountry.dart';
import 'package:flutter_app_seed/dto/DtoCountry.dart';
import 'package:flutter_app_seed/util/AppUtil.dart';
import 'package:flutter_app_seed/util/DialogUtil.dart';
import 'package:flutter_app_seed/widget/DatePicker.dart';

class UiCountryForm extends StatefulWidget {
	final int countryId;

	/// It requires countryId only. The complete data will be fetched from server.
	/// The example of sending complete object is in UiCityForm.
	UiCountryForm({Key key, @required this.countryId}) : super(key: key);

	@override
	UiCountryFormState createState() => UiCountryFormState();
}

class UiCountryFormState extends State<UiCountryForm> {
	final _apiCountry = ApiCountry();
	final _formKey = GlobalKey<FormState>();
	final _scaffoldKey = GlobalKey<ScaffoldState>();
	bool _saving = false;
	bool _loading = false;

	DtoCountry _dto;

	@override
	void initState() {
		super.initState();
		_loadData();
	}

	_loadData() async {
		try {
			setState(() {
				_loading = true;
			});
			_dto = await _apiCountry.findById(widget.countryId);
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		} finally {
			setState(() {
				_loading = false;
			});
		}
	}

	List<DropdownMenuItem> _generateContinentItems() {
		return AppUtil.CONTINENT_LIST
			.map((item) => DropdownMenuItem(value: item, child: Text(item)))
			.toList();
	}

	String _validateRequiredField(String value) {
		if (value.isEmpty) {
			return 'Please enter some text';
		}
		return null;
	}

	void _saveData() async {
		if (_formKey.currentState.validate()) {
			setState(() {
				_saving = true;
			});
			_formKey.currentState.save();
			try {
				await _apiCountry.save(_dto);
				_dto.version = _dto.version + 1;
				_scaffoldKey.currentState.showSnackBar(
					SnackBar(content: Text('The change is successfully saved')));
			} catch (e, s) {
				DialogUtil.handleError(context, e, s);
			} finally {
				setState(() {
					_saving = false;
				});
			}
		}
	}

	@override
	Widget build(BuildContext context) {
//    print('refresh ui $_dto');
		return Scaffold(
			key: _scaffoldKey,
			appBar: AppBar(
				title: Text(_loading ? 'Loading...' : _dto.name),
			),
			body: Container(
				padding: const EdgeInsets.all(16),
				child: _loading
					? Center(
					child: CircularProgressIndicator(),
				)
					: Form(
					key: _formKey,
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.start,
						children: <Widget>[
							TextFormField(
								initialValue: _dto.name,
								validator: _validateRequiredField,
								decoration: InputDecoration(
									labelText: 'Country Name'),
								onSaved: (String value) => _dto.name = value,
							),
							TextFormField(
								initialValue: _dto.capital,
								validator: _validateRequiredField,
								decoration: InputDecoration(
									labelText: 'Capital City'),
								onSaved: (String value) => _dto.capital = value,
							),
							DropdownButtonFormField(
								value: _dto.continent,
								items: _generateContinentItems(),
								decoration: InputDecoration(labelText: 'Continent'),
								isDense: true,
								onChanged: (val) {
									setState(() {
										_dto.continent = val;
									});
								},
							),
							DatePicker(
								labelText: 'Independence Date',
								selectedDate: AppUtil.toDate(_dto.independence),
								selectDate: (DateTime date) {
									setState(() {
										_dto.independence = AppUtil.fromDate(date);
									});
								},
							),
							TextFormField(
								initialValue: '${_dto.population}',
								validator: _validateRequiredField,
								keyboardType: TextInputType.number,
								decoration: InputDecoration(labelText: 'Population'),
								onSaved: (String value) =>
								_dto.population = int.parse(value),
							),
							Padding(
								padding: const EdgeInsets.symmetric(vertical: 16.0),
								child: RaisedButton(
									onPressed: _saving ? null : _saveData,
									child: Text(_saving ? 'Submitting' : 'Submit'),
								),
							),
						],
					),
				)),
			resizeToAvoidBottomInset: false,
		);
	}
}
