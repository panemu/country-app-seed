import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/ApiCountry.dart';
import 'package:flutter_app_seed/dto/DtoCountry.dart';
import 'package:flutter_app_seed/dto/TableData.dart';
import 'package:flutter_app_seed/ui/country/UiCountryForm.dart';
import 'package:flutter_app_seed/util/DialogUtil.dart';
import 'package:flutter_app_seed/widget/AppDrawer.dart';
import 'package:flutter_app_seed/widget/AppNav.dart';

class UiCountryList extends StatefulWidget {
	UiCountryList() : super();

	final String title = "Country List";

	@override
	UiCountryListState createState() => UiCountryListState();
}

class UiCountryListState extends State<UiCountryList>
	with WidgetsBindingObserver {
	ApiCountry _apiCountry = ApiCountry();
	TableData<DtoCountry> _tableData = TableData.empty();
	int _startIndex = 0;
	int _maxRow = 10;
	DtoCountry _selectedDto;
	bool _loading = false;

	@override
	void initState() {
		super.initState();
		WidgetsBinding.instance.addObserver(this);
		_loadData();
	}

	@override
	void dispose() {
		WidgetsBinding.instance.removeObserver(this);
		super.dispose();
	}

	@override
	void didChangeAppLifecycleState(AppLifecycleState state) {
		print('lifecycle ${state.toString()}');
	}

	onSortColumn(int columnIndex, bool ascending) {
		if (columnIndex == 0) {
//      if (ascending) {
//        users.sort((a, b) => a.firstName.compareTo(b.firstName));
//      } else {
//        users.sort((a, b) => b.firstName.compareTo(a.firstName));
//      }
		}
	}

	onSelectedRow(bool selected, DtoCountry country) async {
		setState(() {
			_selectedDto = country;
		});
	}

	deleteSelected() async {}

	_loadData() async {
		try {
			setState(() {
				_selectedDto = null;
				_loading = true;
			});
			_tableData = await _apiCountry.fetch(start: _startIndex, max: _maxRow);
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		} finally {
			setState(() {
				_loading = false;
			});
		}
	}

	_nextPage() {
		_startIndex += _maxRow;
		if (_startIndex > _tableData.totalRows) {
			_startIndex -= _maxRow;
		}

		_loadData();
	}

	_prevPage() {
		_startIndex -= _maxRow;
		_startIndex = max(_startIndex, 0);
		_loadData();
	}

	Widget dataBody() {
		return ListView(
			scrollDirection: Axis.vertical,
			children: [SingleChildScrollView(
				scrollDirection: Axis.horizontal,
				child: DataTable(
					sortColumnIndex: 0,
					columns: [
						DataColumn(label: Text("COUNTRY")),
						DataColumn(
							label: Text("CAPITAL"),
							numeric: false,
						),
						DataColumn(label: Text("CONTINENT")),
					],
					rows: _tableData.rows
						.map(
							(user) =>
							DataRow(
								selected: _selectedDto == user,
								onSelectChanged: (b) {
									onSelectedRow(b, user);
								},
								cells: [
									DataCell(Text(user.name)),
									DataCell(Text(user.capital)),
									DataCell(Text(user.continent)),
								]),
					)
						.toList(),
				)
			)
			]
		);
	}

	_goToDetail() async {
		/**
		 * 1. Show detail window, sends countryId as parameter
		 * 2. Then wait until user back to this window
		 */
		await Navigator.push(
			context,
			new MaterialPageRoute<Null>(
				builder: (BuildContext context) =>
					UiCountryForm(countryId: _selectedDto.id)));

		// 3. Grab the data that was edited by the user from server
		try {
			setState(() {
				_loading = true;
			});

			final dto = await _apiCountry.findById(_selectedDto.id);

			// 4. Replace selectedDto with the fresh data from server
			int indexToReplace = _tableData.rows.indexOf(_selectedDto);
			_tableData.rows[indexToReplace] = dto;
			_selectedDto = dto;
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		} finally {
			setState(() => _loading = false);
		}
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text(widget.title),
				actions: _loading
					? <Widget>[
					Padding(
						padding: EdgeInsets.all(10),
						child: CircularProgressIndicator(
							backgroundColor: Colors.white,
						),
					)
				]
					: null,
			),
			body: Column(
				mainAxisSize: MainAxisSize.min,
				mainAxisAlignment: MainAxisAlignment.center,
				verticalDirection: VerticalDirection.down,
				children: <Widget>[
					Expanded(
						child: dataBody(),
					),
					Row(
						mainAxisAlignment: MainAxisAlignment.center,
						mainAxisSize: MainAxisSize.min,
						children: <Widget>[
							Padding(
								padding: EdgeInsets.all(10.0),
								child: OutlineButton(
									child: Text('Prev'),
									onPressed: _prevPage,
								),
							),
							Padding(
								padding: EdgeInsets.all(10.0),
								child: OutlineButton(
									child: Text('Next'), onPressed: _nextPage),
							),
							Padding(
								padding: EdgeInsets.all(10.0),
								child: OutlineButton(
									child: Text('Detail'),
									onPressed: _selectedDto == null ? null : _goToDetail,
								),
							),
						],
					),
				],
			),
			bottomNavigationBar: AppNav(1),
			drawer: AppDrawer(1),
		);
	}
}
