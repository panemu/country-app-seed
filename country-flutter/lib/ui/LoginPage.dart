import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/ApiUser.dart';
import 'package:flutter_app_seed/api/BaseApi.dart';
import 'package:flutter_app_seed/dto/AuthInfo.dart';
import 'package:flutter_app_seed/localization/AppLocalizations.dart';
import 'package:flutter_app_seed/util/DialogUtil.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
	@override
	_LoginPageState createState() {
		return new _LoginPageState();
	}
}

class _LoginPageState extends State<LoginPage> {
	GlobalKey<FormState> _key = new GlobalKey();
	bool _validate = false;
	LoginRequestData _loginData = LoginRequestData();
	bool _obscureText = true;
	bool loggedIn = false;
	bool _loading = false;

	@override
	void initState() {
		SharedPreferences.getInstance().then((pref) {
			String authInfo = pref.get("auth_info");
			if (authInfo?.isNotEmpty ?? false) {
				print("auth info: $authInfo");
				final JsonDecoder _decoder = JsonDecoder();
				AuthInfo au = AuthInfo(_decoder.convert(authInfo));
				BaseApi.AUTH_TOKEN = au.authToken;
				loggedIn = true;
			} else {
				print("no auth info");
			}
		});

		super.initState();
	}

	@override
	Widget build(BuildContext context) {
		return new Scaffold(
			body: new Center(
				child: new SingleChildScrollView(
					child: new Container(
						margin: new EdgeInsets.all(20.0),
						child: Center(
							child: new Form(
								key: _key,
								autovalidate: _validate,
								child: _getFormUI(),
							),
						),
					),
				),
			),
		);
	}

	Widget _getFormUI() {
		return new Column(
			children: <Widget>[
				new Image.asset(
					'assets/logo.png',
					height: 100,
					width: 100,
				),
				new SizedBox(height: 50.0),
				new TextFormField(
					keyboardType: TextInputType.emailAddress,
					autofocus: false,
					decoration: InputDecoration(
						hintText: 'Email',
						contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
						border:
						OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
					),
					validator: FormValidator().validateEmail,
					onSaved: (String value) {
						_loginData.email = value;
					},
				),
				new SizedBox(height: 20.0),
				new TextFormField(
					autofocus: false,
					obscureText: _obscureText,
					keyboardType: TextInputType.text,
					decoration: InputDecoration(
						hintText: 'Password',
						contentPadding: EdgeInsets.fromLTRB(
							20.0, 10.0, 20.0, 10.0),
						border:
						OutlineInputBorder(
							borderRadius: BorderRadius.circular(32.0)),
						suffixIcon: GestureDetector(
							onTap: () {
								setState(() {
									_obscureText = !_obscureText;
								});
							},
							child: Icon(
								_obscureText ? Icons.visibility : Icons
									.visibility_off,
								semanticLabel:
								_obscureText ? 'show password' : 'hide password',
							),
						),
					),
					validator: FormValidator().validatePassword,
					onSaved: (String value) {
						_loginData.password = value;
					}),
				new SizedBox(height: 15.0),
				new Padding(
					padding: EdgeInsets.symmetric(vertical: 16.0),
					child: _loading
						? CircularProgressIndicator()
						: RaisedButton(
						shape: RoundedRectangleBorder(
							borderRadius: BorderRadius.circular(24),
						),
						onPressed: _sendToServer,
						padding: EdgeInsets.all(12),
						color: Colors.lightBlueAccent,
						child: Text('Log In', style: TextStyle(color: Colors.white)),
					),
				),
				new FlatButton(
					child: Text(
						t(context, 'forgot.password?'),
						style: TextStyle(color: Colors.black54),
					),
					onPressed: _showForgotPasswordDialog,
				),
				new FlatButton(
					onPressed: _sendToRegisterPage,
					child: Text('Not a member? Sign up now',
						style: TextStyle(color: Colors.black54)),
				),
			],
		);
	}

	_sendToRegisterPage() {
//    Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => RegisterPage()),
//    );
	}

	_sendToServer() async {
		if (_key.currentState.validate()) {
			_key.currentState.save();
			if (!loggedIn) {
				try {
					ApiUser apiUser = ApiUser();
					final json = await apiUser.login(
						_loginData.email, _loginData.password);
					final pref = await SharedPreferences.getInstance();
					pref.setString('auth_info', json);

					final JsonDecoder _decoder = JsonDecoder();
					AuthInfo au = AuthInfo(_decoder.convert(json));
					BaseApi.AUTH_TOKEN = au.authToken;
					Navigator.of(context).pushReplacementNamed('/home');
// No any error in validation

					print("Email ${_loginData.email}");
					print("Password ${_loginData.password}");
				} catch (e, s) {
					DialogUtil.handleError(context, e, s);
				}
			}
			/*ApiCountry ac = new ApiCountry();
      ac.fetch(TableQuery()).then((td) {
        print("totalRows ${td.totalRows}");
        td.rows.forEach((item) {
          print("   ${item.name}");
        });
      });*/

		} else {
// validation error
			setState(() {
				_validate = true;
			});
		}
	}

	Future<Null> _showForgotPasswordDialog() async {
		await showDialog<String>(
			context: context,
			builder: (BuildContext context) {
				return new AlertDialog(
					title: const Text('Please enter your eEmail'),
					contentPadding: EdgeInsets.all(5.0),
					content: new TextField(
						decoration: new InputDecoration(hintText: "Email"),
						onChanged: (String value) {
							_loginData.email = value;
						},
					),
					actions: <Widget>[
						new FlatButton(
							child: new Text("Ok"),
							onPressed: () async {
								_loginData.email = "";
								Navigator.pop(context);
							},
						),
						new FlatButton(
							child: new Text("Cancel"),
							onPressed: () => Navigator.pop(context),
						),
					],
				);
			});
	}
}

class LoginRequestData {
	String email = '';
	String password = '';
}

class FormValidator {
	static FormValidator _instance;

	factory FormValidator() => _instance ??= new FormValidator._();

	FormValidator._();

	String validatePassword(String value) {
		if (value.isEmpty) {
			return "Password is Required";
		}
		return null;
	}

	String validateEmail(String value) {
		if (value.isEmpty) {
			return "Email is Required";
		} else {
			return null;
		}
	}
}
