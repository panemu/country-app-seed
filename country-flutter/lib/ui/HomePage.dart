import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/ApiCity.dart';
import 'package:flutter_app_seed/api/BaseApi.dart';
import 'package:flutter_app_seed/localization/AppLocalizations.dart';
import 'package:flutter_app_seed/util/DialogUtil.dart';
import 'package:flutter_app_seed/widget/AppDrawer.dart';
import 'package:flutter_app_seed/widget/AppNav.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
	@override
	State createState() {
		return _HomePageState();
	}
}

class _HomePageState extends State<HomePage> {

	ApiCity _api = ApiCity();

	Widget _createButton(String text, Function f) {
		return RaisedButton(
			child: Text(text),
			onPressed: f,
		);
	}

	@override
	Widget build(BuildContext context) =>
		new Scaffold(
			appBar: new AppBar(
				title: new Text('Home'),
			),
			body: new Container(
				margin: new EdgeInsets.only(top: 50.0),
				alignment: Alignment.center,
				child: new Column(
					children: <Widget>[
						new Text(t(context, 'greeting')),
						new FlatButton(
							child: new Text('Logout'),
							onPressed: () async {
								_logout(context);
							}),
						_createButton(t(context, "show.info.dialog"), _showInfoDialog),
						_createButton("Error Dialog", _showErrorDialog),
						_createButton(
							"Confirmation Dialog", _showConfirmationDialog),
						_createButton("Custom Dialog", _showCustomDialog),
						_createButton("Test Error With Param", _testErrorHandling),
						_createButton("Test Error No Param", _testErrorHandlingNoParam),
						_createButton("Unexpected Error", _testUnexpectedError),
						_createButton("Unauthorized Error", _testUnautorizedError),
					],
				),
			),
			bottomNavigationBar: AppNav(0),
			drawer: AppDrawer(0),
		);

	_logout(BuildContext context) {
		SharedPreferences.getInstance().then((pref) {
			pref.remove('auth_info');
			BaseApi.AUTH_TOKEN = '';
			Navigator.of(context).pushReplacementNamed('/login');
		});
	}

	_showInfoDialog() async {
		final answer =
		await DialogUtil.showMessageDialog(context, 'This is the message');
		print('answer $answer');
	}

	_showErrorDialog() async {
		final answer =
		await DialogUtil.showErrorDialog(context, 'Bad things happened.');
		print('answer $answer');
	}

	_showConfirmationDialog() async {
		final answer = await DialogUtil.showConfirmationDialog(
			context, "Do you want a YES or a NO?");
		print('answer $answer');
	}

	_showCustomDialog() async {
		final answer = await DialogUtil.showConfirmationDialog(
			context,
			'Update is available.'
				'Do you want to install it?',
			title: 'Update',
			yesOkLabel: 'Install Now',
			noLabel: 'Ignore',
			cancelLabel: 'Remind me later');
		print('answer $answer');
	}

	_testErrorHandling() async {
		try {
			await _api.testErrorWithParam();
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		}
	}

	_testErrorHandlingNoParam() async {
		try {
			await _api.testErrorNoParam();
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		}
	}

	_testUnexpectedError() async {
		try {
			await _api.testUnexpectedError();
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		}
	}

	_testUnautorizedError() async {
		try {
			await _api.testUnauthorizedError();
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		}
	}
}
