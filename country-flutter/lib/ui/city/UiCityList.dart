import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/ApiCity.dart';
import 'package:flutter_app_seed/dto/DtoCity.dart';
import 'package:flutter_app_seed/dto/TableData.dart';
import 'package:flutter_app_seed/ui/city/UiCityForm.dart';
import 'package:flutter_app_seed/util/DialogUtil.dart';
import 'package:flutter_app_seed/widget/AppDrawer.dart';
import 'package:flutter_app_seed/widget/AppNav.dart';

class UiCityList extends StatefulWidget {
	@override
	UiCityListState createState() => UiCityListState();
}

class UiCityListState extends State<UiCityList> {
	List<DtoCity> _lst = [];
	ScrollController _scrollController = new ScrollController();
	bool _isLoading = false;
	int _startIndex = 0;
	static const int _recordPerPage = 20;
	final _refreshIndicatorState = GlobalKey<RefreshIndicatorState>();
	final ApiCity _apiCity = ApiCity();
	@override
	void initState() {
		super.initState();
//		_loadData();
		WidgetsBinding.instance
			.addPostFrameCallback((_) =>
			_refreshIndicatorState.currentState.show());
		_scrollController.addListener(() {
			if (_scrollController.position.pixels ==
				_scrollController.position.maxScrollExtent) {
				_startIndex += _recordPerPage;
				_loadData(false);
			}
		});
	}


	_loadData(bool clear) async {
		if (_isLoading) {
			print('still loading. new loading request rejected');
			return;
		}
		try {
			setState(() {
				_isLoading = true;
			});

			print('load from $_startIndex');
			final TableData<DtoCity> td =
			await _apiCity.fetch(start: _startIndex, max: _recordPerPage);
			setState(() {
				if (clear) _lst.clear();
				_lst.addAll(td.rows);
				_isLoading = false;
			});
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		}
	}

	_goToDetail(DtoCity city) async {
		final result = await Navigator.push(context, new MaterialPageRoute(
			builder: (BuildContext context) {
				return UiCityForm(city: city);
			},
		));

		int indexToReplace = _lst.indexOf(city);
		if (indexToReplace >= 0 && result is DtoCity) {
			_lst[indexToReplace] = result;
		}
		print('got value from form screen $result');
	}

	@override
	void dispose() {
		_scrollController.dispose();
		super.dispose();
	}

	Future<void> _handleRefresh() async {
		_startIndex = 0;
		await _loadData(true);
	}

	_delete(int index) async {
		await _apiCity.delete(_lst[index].id);
		setState(() {
			_lst.removeAt(index);
		});
	}

	_buildList() {
		return RefreshIndicator(
			key: _refreshIndicatorState,
			onRefresh: _handleRefresh,
			child: ListView.separated(
				padding: const EdgeInsets.symmetric(horizontal: 20),
				itemCount: _lst.length + 1,
				separatorBuilder: (context, index) =>
					Divider(
						color: Colors.black,
					),
				itemBuilder: (context, index) {
					if (_lst.isEmpty) {
						return Container();
					}
					if (index == _lst.length) {
						return new Padding(
							padding: const EdgeInsets.all(8.0),
							child: new Center(
								child: new Opacity(
									opacity: _isLoading ? 1.0 : 00,
									child: new CircularProgressIndicator(),
								),
							),
						);
					} else {
						return Dismissible(
							key: Key(_lst[index].id.toString()),
							confirmDismiss: (direction) async {
								Answer ans = await DialogUtil.showConfirmationDialog(context, "Do you want to delete ${_lst[index].name}?");
								return ans == Answer.yes_ok;
							},
							onDismissed: (direction) => _delete(index),
							child: ListTile(
								title: Text(_lst[index].name),
								subtitle: Text(_lst[index].countryName),
								trailing: Icon(Icons.arrow_right),
								onTap: () {
									_goToDetail(_lst[index]);
								},
							),
						);
					}
				},
				controller: _scrollController,
			));
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text('Cities'),
				actions: <Widget>[
					IconButton(
						icon: Icon(Icons.search),
						onPressed: ()=> print('dosearch'),
					),
				],
			),
			body: Container(child: _buildList()),
			floatingActionButton: FloatingActionButton(
				onPressed: () {
					final dto = DtoCity();
					dto.id = 0;
					dto.version = 0;
					_goToDetail(dto);
				},
				child: Icon(Icons.add),
			),
			bottomNavigationBar: AppNav(2),
			drawer: AppDrawer(2),
		);
	}
}
