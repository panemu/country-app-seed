import 'package:flutter/material.dart';
import 'package:flutter_app_seed/api/ApiCity.dart';
import 'package:flutter_app_seed/dto/DtoCity.dart';
import 'package:flutter_app_seed/dto/DtoCountryOption.dart';
import 'package:flutter_app_seed/util/AppUtil.dart';
import 'package:flutter_app_seed/util/DialogUtil.dart';

class UiCityForm extends StatefulWidget {
	final DtoCity city;

	/// The complete data is sent from parent page. To see the example of only
	/// id, check UiCountryForm
	///
	UiCityForm({Key key, @required this.city}) : super(key: key);

	@override
	UiCityFormState createState() => UiCityFormState();
}

class UiCityFormState extends State<UiCityForm> {
	DtoCity _dto;
	ApiCity _apiCity = ApiCity();
	final _scaffoldKey = GlobalKey<ScaffoldState>();
	final _formKey = GlobalKey<FormState>();
	bool _loading = false;
	List<DtoCountryOption> _lstCountry = [];

	@override
	void initState() {
		super.initState();
		_dto = widget.city.clone();
		_onContinentChange(_dto.continent);
	}

	String _validateRequiredField(String value) {
		if (value.isEmpty) {
			return 'Please enter some text';
		}
		return null;
	}

	void _saveData() async {
		if (_formKey.currentState.validate()) {
			setState(() {
				_loading = true;
			});
			_formKey.currentState.save();
			try {
				await _apiCity.save(_dto);

				_dto.version += 1; //it is to avoid optimistic locking exception in server.

				_scaffoldKey.currentState.showSnackBar(
					SnackBar(content: Text('The change is successfully saved')));
			} catch (e, s) {
				DialogUtil.handleError(context, e, s);
			}
			setState(() {
				_loading = false;
			});
		}
	}

	Future<bool> _willPop() async {
		print('$runtimeType _willPop');
		Navigator.pop(context, _dto);
		return false;
	}

	List<DropdownMenuItem> _generateContinentItems() {
		return AppUtil.CONTINENT_LIST
			.map((item) => DropdownMenuItem(value: item, child: Text(item)))
			.toList();
	}

	void _onContinentChange(String newContinent) async {
		setState(() {
			_lstCountry = [];
			if (_dto.continent != newContinent) {
				_dto.countryName = null;
			}
			_dto.continent = newContinent;
			_loading = true;
		});

		try {
			List<DtoCountryOption> result = await _apiCity
				.findCountriesByContinent(_dto.continent);
			_lstCountry = result;
			print('lst country size ${_lstCountry.length}');
		} catch (e, s) {
			DialogUtil.handleError(context, e, s);
		} finally {
			setState(() {
				_loading = false;
			});
		}
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			key: _scaffoldKey,
			appBar: AppBar(
				title: Text(_dto.name == null? 'New City' : _dto.name),
				actions: _loading
					? <Widget>[
					Padding(
						padding: EdgeInsets.all(10),
						child: CircularProgressIndicator(
							backgroundColor: Colors.white,
						),
					)
				]
					: null,
			),
			body: Container(
				padding: const EdgeInsets.all(16),
				child: Form(
					key: _formKey,
					child: Column(
						crossAxisAlignment: CrossAxisAlignment.start,
						children: <Widget>[
							TextFormField(
								initialValue: _dto.name,
								validator: _validateRequiredField,
								decoration: InputDecoration(labelText: 'City Name'),
								onSaved: (String value) => _dto.name = value,
							),
							DropdownButtonFormField<String>(
								value: _dto.continent,
								items: _generateContinentItems(),
								isDense: true,
								decoration: InputDecoration(
									labelText: 'Continent'),
								onChanged: _onContinentChange
							),

							DropdownButtonFormField(
								value: _dto.countryName,
								items: _lstCountry.map((item) =>
									DropdownMenuItem(value: item.name,
										child: Text(item.name))).toList(),
//									items: _generateContinentItems(),
								decoration: InputDecoration(labelText: 'Country'),
								isDense: true,
								onChanged: (val) {
									setState(() {
										_dto.countryName = val;
										DtoCountryOption selected = _lstCountry
											.firstWhere((item) => item.name == val);
										_dto.countryId = selected.id;
									});
								},
							),
							Padding(
								padding: const EdgeInsets.symmetric(vertical: 16.0),
								child: RaisedButton(
									onPressed: _loading ? null : _saveData,
									child: Text('Submit'),
								),
							)
						]),
					onWillPop: _willPop,
				),
			),
			resizeToAvoidBottomInset: false,
		);
	}
}
