class TableData<T> {
	List<T> rows;
	int totalRows;

	TableData(this.rows, this.totalRows);

	TableData.empty() {
		rows = [];
		totalRows = 0;
	}

	TableData.fromMap(Map<String, dynamic> map, Function(List) f)
		: rows = f(map['rows']),
			totalRows = map['totalRows'];
}
