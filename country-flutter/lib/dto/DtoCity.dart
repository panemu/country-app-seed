class DtoCity {
	String continent;
	int countryId;
	String countryName;
	int id;
	String name;
	int version;

	DtoCity({this.continent,
		this.countryId,
		this.countryName,
		this.id,
		this.name,
		this.version});

	DtoCity clone() {
		return DtoCity.fromJson(this.toJson());
	}

	DtoCity.fromJson(Map<String, dynamic> json)
		: continent = json['continent'],
			countryId = json['countryId'],
			countryName = json['countryName'],
			id = json['id'],
			name = json['name'],
			version = json['version'];

	Map<String, dynamic> toJson() =>
		{
			'continent': continent,
			'countryId': countryId,
			'countryName': countryName,
			'id': id,
			'name': name,
			'version': version,
		};

	@override
	String toString() {
		return 'DtoCity{continent: $continent, countryId: $countryId, countryName: $countryName, id: $id, name: $name, version: $version}';
	}
}
