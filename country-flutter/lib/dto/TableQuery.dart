class TableQuery {
	List<TableCriteria> tableCriteria = new List();
	List<SortingInfo> sortingInfos = new List();

	Map<String, dynamic> toJson() =>
		{'tableCriteria': tableCriteria, 'sortingInfos': sortingInfos};
}

class TableCriteria {
	String attributeName;
	String value;

	Map<String, dynamic> toJson() =>
		{'attributeName': attributeName, 'value': value};
}

class SortingInfo {
	String attributeName;
	String direction;

	Map<String, dynamic> toJson() =>
		{'attributeName': attributeName, 'direction': direction};
}
