class AuthInfo {
	String authToken;
	int id;
	String role;
	String username;
	List<String> permissions;

	static final AuthInfo _instance = AuthInfo._privateConstructor();

	AuthInfo._privateConstructor();

	factory AuthInfo(Map<String, dynamic> map) {
		_instance.authToken = map['authToken'];
		_instance.id = map['id'];
		_instance.role = map['role'];
		_instance.username = map['username'];
		final List<dynamic> lstPerm = map['permissions'];
		_instance.permissions = lstPerm.map((item) => item.toString()).toList();
		return _instance;
	}

	static AuthInfo get instance {
		return _instance;
	}
}
