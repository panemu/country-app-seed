class DtoCountryOption {
	int id;
	String name;

	DtoCountryOption.fromJson(Map<String, dynamic> json)
		: id = json['id'],
			name = json['name'];

	Map<String, dynamic> toJson() =>
		{
			'id': id,
			'name': name,
		};


}