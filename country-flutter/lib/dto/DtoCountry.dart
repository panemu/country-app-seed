class DtoCountry {
	String capital;
	String continent;
	int id;
	String independence;
	String name;
	int population;
	int version;
	bool selected = false;

	static const _fieldList = [
		'capital',
		'continent',
		'id',
		'independence',
		'name',
		'population',
		'version'
	];

	DtoCountry({this.capital,
		this.id,
		this.independence,
		this.name,
		this.population,
		this.version});

	DtoCountry.fromJson(Map<String, dynamic> json)
		: capital = json['capital'],
			continent = json['continent'],
			id = json['id'],
			independence = json['independence'],
			name = json['name'],
			population = json['population'],
			version = json['version'];

	Map<String, dynamic> toJson() =>
		{
			'capital': capital,
			'continent': continent,
			'id': id,
			'independence': independence,
			'name': name,
			'population': population,
			'version': version,
		};

	DtoCountry clone() {
		DtoCountry d = DtoCountry.fromJson(this.toJson());
		return d;
	}

	@override
	String toString() {
		return 'DtoCountry{capital: $capital, continent: $continent, id: $id, independence: $independence, name: $name, population: $population, version: $version, selected: $selected}';
	}
}
