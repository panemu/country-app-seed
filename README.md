# Country App Seed ([Demo](https://scs-demo.panemu.com/country))

This Project is an application skeleton Web App that using Java / Jakarta EE as backend and Custom Component from Angular 8 Framework. The Backend and Frontend can work separately, so you can use our custom component with your own Backend API and vice versa.

The Custom Component has rich capability especially TableData that has pagination, sorting, show-hide column, ordering column, export to excel, etc.

The Backend architecture is designed by [Panemu](https://panemu.com) to deal with your common feature such as CRUD Operations, User Authentication/Authorization and also Error Handling.

### Prerequisites

* [Git](https://git-scm.com/) to clone this project.
* [Node & Npm](https://nodejs.org/) (latest LTS version is recomended)

If you are a Java Programmer or want to use **country-server** as your backend, you will also need:

* [Wildfly v.16](http://wildfly.org/)
* Java / Jakarta EE 8 or 11
* Maven
* Mysql Database

### Clone `country-app-seed`

Clone the `country-app-seed` repository using git:

```
git clone https://bitbucket.org/panemu/country-app-seed.git
cd country-app-seed
```
After that you will see two projects **country-web** and **country-server** then you can open its with your favorite IDE.

### Use Country API
If you are a Frontend Programmer and won't to deal with database and other backend configurations, we provide API for you. Just open file environment.ts
inside folder `src/environments` in **country-web** project and change API url into :

```
 API: 'https://scs-demo.panemu.com/country/api/'
```

Country web is ready to rock and roll with `npm-start` then login with username & password: **hellocountry**.

### Database Configuration
Create database:

```
CREATE DATABASE country;
```

Create a user that has full access to that database:

```
GRANT ALL PRIVILEGES ON country.* To 'country'@'localhost' IDENTIFIED BY 'yourpassword';

```

We have provided `country.sql` in the root of the project. Import the sql file to the database using this command:

`mysql -uroot -p country < country.sql`

### Wildfly Datasource Configuration
Country use [Datasource](https://docs.jboss.org/author/display/WFLY10/DataSource+configuration?_sscc=t) to connect database, you need to install MySQL in Wildfly.

Download MySQL connector [here](https://dev.mysql.com/downloads/connector/j/8.0.html), then create this folder :

`wildfly-16.0.0.Final/modules/system/layers/base/com/mysql/driver8/main`

Put MySQL connector jar in it. Use the lates version, it is 8.0.13 when this document is written.
Create a module.xml file and put these lines:

```
<module xmlns="urn:jboss:module:1.3" name="com.mysql.driver8">

    <resources>
        <resource-root path="mysql-connector-java-8.0.16.jar"/>
    </resources>
    <dependencies>
        <module name="javax.api"/>
        <module name="javax.transaction.api"/>
    </dependencies>
</module>
```

Edit <wildfly>/standalone/configuration/standalone.xml file. Add datasource and driver parts of mysql below:

```
<datasources>
	<datasource jta="true" jndi-name="java:jboss/datasources/Country_DS" pool-name="Country_DS" enabled="true" use-java-context="true" use-ccm="true">
        <connection-url>jdbc:mysql://localhost:3306/country?useSSL=false&amp;useUnicode=yes&amp;characterEncoding=UTF-8&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=Asia/Jakarta</connection-url>
	    <driver>mysql8</driver>
	    <security>
	    	<user-name>country</user-name>
	        <password>yourpassword</password>
		</security>
	    <statement>
	    	<prepared-statement-cache-size>32</prepared-statement-cache-size>
	        <share-prepared-statements>true</share-prepared-statements>
		</statement>
	</datasource>
	 <drivers>
        <driver name="mysql8" module="com.mysql.driver8">
           <driver-class>com.mysql.cj.jdbc.Driver</driver-class>
           <xa-datasource-class>com.mysql.cj.jdbc.MysqlXADataSource</xa-datasource-class>
        </driver>
    </drivers>
</datasources>
```

Add JNDI for upload folder location. Modify the folder location as needed. **Change the salt for token auth**.

```
<subsystem xmlns="urn:jboss:domain:naming:2.0">
	<bindings>
		...
		<simple name="java:global/country/upload-folder" value="/home/myfiles/country/upload/" type="java.lang.String"/>
		<simple name="java:global/country/salt" value="SALT_FOR_TOKEN_AUTH" type="java.lang.String"/>
		...
	</bindings>
</subsystem>
```


**IMPORTANT**
Open shiro.ini file and change the cipher key below with your own key:
`securityManager.rememberMeManager.cipherKey = kPH+bIxj368dgZiIxcaaaA==`

Also delete hellocountry user from database.

### Getting started

If you are a Frontend Programmer and won't to deal with database and other backend configurations, we provide API for you. Just open file environtment.ts
inside folder environtments in country-web project and change API url into :

```
 API: 'https://scs-demo.panemu.com/country/api/'
```

Run `npm start` for a dev server. Navigate to `http://localhost:4200/` in your browser.

### Building the project

**1. country-server**

```mvn clean install```

Run project from your IDE.

**2. country-web**

Run `npm start` for a dev server. Navigate to `http://localhost:4200/` in your browser. Then login using username & password: **hellocountry**.


### Main Features:

* User authorization and authentication using PBAC (permission based access control). User can create new role and assign permissions to it.
* CRUD for all modules.
* Data pagination.
* Save table layout (width, column visibility, column order) in local storage. If using Java backend, user can also save it to database.
* [Advanced Search](https://scs-demo.panemu.com/country/home/help).
* Export data to xls format.
* Support multiple languages.
* Localized/Internationalized Error Handling.


### Disclaimer

Data in this application is dummy. The information might not represent actual fact.

### Demo

Link demo is [here](https://scs-demo.panemu.com/country).

Video demo is [here](https://www.youtube.com/watch?v=IhSzEi2osGw).

Last but not least [video installation](https://youtu.be/s9AbwJyY9Hg).

### License

[BSD LICENSE](https://bitbucket.org/panemu/country-app-seed/src/master/LICENSE)

