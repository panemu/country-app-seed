package com.panemu.country.ws;

import com.panemu.country.common.CommonUtil;
import com.panemu.country.dto.DtoAuth;
import com.panemu.country.dto.DtoLogin;
import com.panemu.country.rcd.LogSystem;
import com.panemu.country.rcd.Member;
import com.panemu.country.security.SaltedJdbcRealm;
import com.panemu.country.srv.SrvLogSystem;
import com.panemu.country.srv.SrvMember;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.Subject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import static org.powermock.api.mockito.PowerMockito.*;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author amrullah
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({SecurityUtils.class, SaltedJdbcRealm.class})
public class TstWsAuth {
	@Mock
	private HttpServletRequest request;
	private final String requestIpAddress = "10.0.0.1";
	
	@Before
	public void initTest() {
		mockStatic(SecurityUtils.class);
		when(request.getHeader("X-FORWARDED-FOR")).thenReturn(requestIpAddress);
	}
	
	@Test
	public void login_success() {
		final String username = "mockuser";
		final String permission = "user:maintenance";
		final String role = "admin";
		
		mockStatic(SaltedJdbcRealm.class);
		Subject subject = mock(Subject.class);
		SimpleAuthorizationInfo authInfo = new SimpleAuthorizationInfo();
		
		authInfo.setRoles(Sets.newSet("admin"));
		authInfo.setStringPermissions(Sets.newSet(permission));
		when(subject.isAuthenticated()).thenReturn(Boolean.FALSE);
		
		when(SecurityUtils.getSubject()).thenReturn(subject);
		SaltedJdbcRealm realm = mock(SaltedJdbcRealm.class);
		when(SaltedJdbcRealm.getInstance()).thenReturn(realm);
		when(realm.getSubjectInfo(subject)).thenReturn(authInfo);
		
		SrvMember srvMember = mock(SrvMember.class);
		SrvLogSystem srvLog = mock(SrvLogSystem.class);
		Member member = new Member(1, username, "", "", 1, null, null, role);
		when(srvMember.getMemberByMemberName(username)).thenReturn(member);
		WsAuth ws = new WsAuth();
		ws.srvMember = srvMember;
		ws.srvLog = srvLog;
		DtoLogin dtoLogin = new DtoLogin(username, "");
		final DtoAuth loginResult = ws.login(dtoLogin, request);
		
		//return correct data
		Assert.assertEquals(loginResult.getUsername(), username);
		Assert.assertEquals(loginResult.getRole(), role);
		Assert.assertArrayEquals(new String[]{permission}, loginResult.getPermissions().toArray());
		Assert.assertTrue(StringUtils.isBlank(loginResult.getAuthToken()));
		
		//log login activity
		ArgumentCaptor<LogSystem> logSysArg = ArgumentCaptor.forClass(LogSystem.class);
		Mockito.verify(srvLog, Mockito.times(1)).insertLogSystem(logSysArg.capture());
		Assert.assertTrue(StringUtils.contains(logSysArg.getValue().getDescription(), requestIpAddress));
	}
	
	@Test
	public void loginFromMobile() {
		final String username = "mockuser";
		final String permission = "user:maintenance";
		final String role = "admin";

		mockStatic(SaltedJdbcRealm.class);
		Subject subject = mock(Subject.class);
		SimpleAuthorizationInfo authInfo = new SimpleAuthorizationInfo();

		authInfo.setRoles(Sets.newSet("admin"));
		authInfo.setStringPermissions(Sets.newSet(permission));
		when(subject.isAuthenticated()).thenReturn(Boolean.FALSE);

		when(SecurityUtils.getSubject()).thenReturn(subject);
		SaltedJdbcRealm realm = mock(SaltedJdbcRealm.class);
		when(SaltedJdbcRealm.getInstance()).thenReturn(realm);
		when(realm.getSubjectInfo(subject)).thenReturn(authInfo);
		
//		mockStatic(CommonUtil.class);
		CommonUtil.setSalt("jkl*989 jkapppaanneemmu");

		SrvMember srvMember = mock(SrvMember.class);
		SrvLogSystem srvLog = mock(SrvLogSystem.class);
		Member member = new Member(1, username, "", "", 1, null, null, role);
		when(srvMember.getMemberByMemberName(username)).thenReturn(member);
		WsAuth ws = new WsAuth();
		ws.srvMember = srvMember;
		ws.srvLog = srvLog;
		DtoLogin dtoLogin = new DtoLogin(username, "");
		final DtoAuth loginResult = ws.loginFromMobile(dtoLogin, request);
		
		/**
		 * - srvMember.updateMember is called
		 * - member.authToken equals digest of loginResult.authToken
		 * - loginResult has correct role and permission
		 */
		ArgumentCaptor<Member> memberArg = ArgumentCaptor.forClass(Member.class);
		Mockito.verify(srvMember, Mockito.times(1)).updateMember(memberArg.capture());
		String expectedDigest = CommonUtil.digestToken(loginResult.getAuthToken());
		Assert.assertEquals(expectedDigest, memberArg.getValue().getAuthToken());
		Assert.assertEquals(loginResult.getUsername(), username);
		Assert.assertEquals(loginResult.getRole(), role);
		Assert.assertArrayEquals(new String[]{permission}, loginResult.getPermissions().toArray());
		Assert.assertTrue(StringUtils.isNotBlank(loginResult.getAuthToken()));

		//log login activity
		ArgumentCaptor<LogSystem> logSysArg = ArgumentCaptor.forClass(LogSystem.class);
		Mockito.verify(srvLog, Mockito.times(1)).insertLogSystem(logSysArg.capture());
		Assert.assertTrue(StringUtils.contains(logSysArg.getValue().getDescription(), requestIpAddress));
	}
	
	@Test
	public void login_failed() {
		Subject subject = mock(Subject.class);
		when(SecurityUtils.getSubject()).thenReturn(subject);
		ArgumentCaptor<UsernamePasswordToken> loginToken = ArgumentCaptor.forClass(UsernamePasswordToken.class);
		doThrow(new AuthenticationException()).when(subject).login(loginToken.capture());
		WsAuth ws = new WsAuth();
		try {
			ws.login(new DtoLogin("baduser", ""), request);
			Assert.fail("the login should throw exception");
		} catch (Exception ex) {
			Assert.assertEquals(ex.getClass(), AuthorizationException.class);
			Assert.assertEquals("baduser not authenticated. invalid username or password", ex.getMessage());
		}
	}
}

