package com.panemu.country.ws;

import com.panemu.country.common.CommonUtil;
import com.panemu.country.common.TableData;
import com.panemu.country.error.ErrorCode;
import com.panemu.country.rcd.Asset;
import com.panemu.country.security.NeedAuthentication;
import com.panemu.country.security.NeedPermissions;
import com.panemu.country.srv.SrvAsset;
import com.panemu.search.TableQuery;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.util.CollectionUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@Path("asset")
@Stateless
public class WsAsset {

	private Logger log = LoggerFactory.getLogger(WsAsset.class);

	@Inject
	private SrvAsset srvAsset;

	@Resource(lookup = "java:global/country/upload-folder")
	private String uploadFolder;

	@GET
	@Path("test")
	@Produces({MediaType.APPLICATION_JSON})
	public Map testApi() {
		Map<String, Object> m = new HashMap<>();
		m.put("message", "this is from vscode, auto deploy");
		return m;
	}

	@GET
	@Path("{category}/{path}")
	@NeedAuthentication
	public Response getAsset(@PathParam("category") String category, @PathParam("path") String path, @QueryParam("id") int id) {
		String filename = path;
		if (id > 0) {
			Asset asset = srvAsset.findById(Asset.class, id);
			if (asset != null) {
				filename = asset.getFilename();
				if (!asset.getCategory().equals(category) || !asset.getPath().equals(path)) {
					throw ErrorCode.ER0400.exception("Invalid asset information");
				}
			}
		}
		if (!uploadFolder.endsWith(File.separator)) {
			uploadFolder = uploadFolder + File.separator;
		}
		File file = new File(uploadFolder + category + File.separator + path);
		Response.ResponseBuilder response = Response.ok((Object) file);
		response.header("Content-Disposition", "attachment; filename=" + filename);
		return response.build();
	}

	@POST
	@NeedPermissions(value = {"asset:read", "asset:write"}, logical = Logical.OR)
	public TableData<Asset> findAll(@QueryParam("start") int startIndex, @QueryParam("max") int maxRecord, TableQuery tq) {
		TableData<Asset> lstResult = srvAsset.find(tq, startIndex, maxRecord);
		return lstResult;
	}

	@POST
	@Path("upload")
	@Consumes("multipart/form-data")
	@NeedAuthentication
	@Produces({MediaType.APPLICATION_JSON})
	public Asset uploadFile(MultipartFormDataInput input) throws IOException {
		String random = "_" + RandomStringUtils.randomAlphabetic(3);
		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		Asset asset = new Asset();

		// Get file data to save
		List<InputPart> inputParts = uploadForm.get("attachment");

		List<InputPart> lstItem = uploadForm.get("category");
		if (lstItem == null || lstItem.isEmpty()) {
			throw ErrorCode.ER0400.exception("Category DB Asset cannot be blank.");
		} else {
			String category = lstItem.get(0).getBodyAsString();
			if (StringUtils.isBlank(category)) {
				throw ErrorCode.ER0400.exception("\"category\" parameter cannot be blank.");
			}
			asset.setCategory(category.toLowerCase());
		}

		lstItem = uploadForm.get("refId");
		if (lstItem == null || lstItem.isEmpty()) {
			throw ErrorCode.ER0400.exception("\"refId\" parameter cannot be blank.");
		} else {
			String refId = lstItem.get(0).getBodyAsString();
			asset.setRefId(Integer.parseInt(refId));
		}

		String subCategory = "";
		lstItem = uploadForm.get("subCategory");
		if (!CollectionUtils.isEmpty(lstItem)) {
			subCategory = lstItem.get(0).getBodyAsString();
			subCategory = subCategory == null ? "" : subCategory.trim();
			if (StringUtils.isNotBlank(subCategory)) {
				asset.setSubCategory(subCategory);
				subCategory = "_" + subCategory;
			}
		}

		boolean overwrite = false;
		lstItem = uploadForm.get("overwrite");
		if (!CollectionUtils.isEmpty(lstItem)) {
			String ow = lstItem.get(0).getBodyAsString();
			overwrite = "TRUE".equalsIgnoreCase(ow);
		}

		if (overwrite) {
			List<Asset> assetToDel = srvAsset.getAssetToOverwrite(asset.getCategory(), asset.getRefId(), asset.getSubCategory());
			if (assetToDel != null) {
				for (Asset todel : assetToDel) {
					srvAsset.deleteAsset(todel);
				}
			}
		}

		lstItem = uploadForm.get("description");
		if (lstItem != null && !lstItem.isEmpty()) {
			String keterangan = lstItem.get(0).getBodyAsString();
			asset.setDescription(keterangan);
		}

		asset.setModifiedOn(new Date());
		asset.setModifiedBy(String.valueOf(SecurityUtils.getSubject().getPrincipal()));

		if (!CollectionUtils.isEmpty(inputParts)) {
			if (!uploadFolder.endsWith(File.separator)) {
				uploadFolder = uploadFolder + File.separator;
			}
			InputStream inputStream = null;
			try {
				InputPart inputPart = inputParts.get(0);
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				String fileName = getFileName(header);
				asset.setFilename(fileName);
				String extension = CommonUtil.getExtensionWithDot(fileName);
				inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = IOUtils.toByteArray(inputStream);

				String folderPath = uploadFolder + asset.getCategory();
				File folder = new File(folderPath);
				if (!folder.exists()) {
					folder.mkdirs();
				}

				asset = srvAsset.insert(asset);
				String path = asset.getRefId() + "_" + asset.getId() + subCategory + random + extension;
				asset.setPath(path);

				fileName = folderPath + File.separator + asset.getPath();
				writeFile(bytes, fileName);
				if (CommonUtil.isImageExtension(extension) && ImageIO.read(new File(fileName)) != null) {
					asset.setPathSmall(asset.getRefId()+ "_" + asset.getId() + subCategory + random + "_small" + extension);
					String pathSmall = folderPath + File.separator + asset.getPathSmall();
					createSmallPicture(fileName, pathSmall);
				}
				srvAsset.update(asset);
//				if ("teknologi".equalsIgnoreCase(asset.getCategory())) {
//					srvTeknologi.updateTeknologiAsset(asset);	
//				}
//				if ("rekomendasi".equalsIgnoreCase(asset.getCategory())) {
//					srvRekomendasi.updateAssetInfo(asset.getRefId(), asset.getPath());
//				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				throw ErrorCode.ER0400.exception(e.getMessage());
			} finally {
				if (inputStream != null) {
					inputStream.close();
				}
			}
		}

		return asset;
	}

	@PUT
	@NeedAuthentication
	@Produces({MediaType.APPLICATION_JSON})
	public List<Asset> saveAsset(List<Asset> assets) {
		List<Asset> result = new ArrayList<>();
		for (Asset asset : assets) {
			asset.setModifiedOn(new Date());
			asset.setModifiedBy(String.valueOf(SecurityUtils.getSubject().getPrincipal()));
			asset = srvAsset.update(asset);
			result.add(asset);
		}
		return result;
	}

	@DELETE
	@Path("{ids}")
	@NeedAuthentication
	@Produces({MediaType.APPLICATION_JSON})
	public void deleteAsset(@PathParam("ids") String stringIds) {
		log.info("Delete assets: " + stringIds);
		String[] arrIds = stringIds.split(",");
		for (String stringid : arrIds) {
			int id = NumberUtils.toInt(stringid, 0);
			if (id > 0) {
				Asset asset = srvAsset.findById(Asset.class, id);
				srvAsset.deleteAsset(asset);
			}
		}
	}

	private void createSmallPicture(String sourcePict, String pathSmall) throws IOException {
		BufferedImage originalBufferedImage = null;
		try {
			originalBufferedImage = ImageIO.read(new File(sourcePict));
		} catch (IOException ioe) {
			throw ioe;
		}

		int thumbnailWidth = 100;

		int widthToScale, heightToScale;
		if (originalBufferedImage.getWidth() > originalBufferedImage.getHeight()) {

			heightToScale = (int) (1.1 * thumbnailWidth);
			widthToScale = (int) ((heightToScale * 1.0) / originalBufferedImage.getHeight()
					  * originalBufferedImage.getWidth());

		} else {
			widthToScale = (int) (1.1 * thumbnailWidth);
			heightToScale = (int) ((widthToScale * 1.0) / originalBufferedImage.getWidth()
					  * originalBufferedImage.getHeight());
		}

		BufferedImage resizedImage = new BufferedImage(widthToScale,
				  heightToScale, originalBufferedImage.getType());
		Graphics2D g = resizedImage.createGraphics();

		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.drawImage(originalBufferedImage, 0, 0, widthToScale, heightToScale, null);
		g.dispose();
		int x = (resizedImage.getWidth() - thumbnailWidth) / 2;
		int y = (resizedImage.getHeight() - thumbnailWidth) / 2;

		if (x < 0 || y < 0) {
			throw new IllegalArgumentException("Width of new thumbnail is bigger than original image");
		}
		BufferedImage thumbnailBufferedImage = resizedImage.getSubimage(x, y, thumbnailWidth, thumbnailWidth);
		try {
			String extension = CommonUtil.getExtension(sourcePict);
			ImageIO.write(thumbnailBufferedImage, extension, new File(pathSmall));
		} catch (IOException ioe) {
			throw ioe;
		}
	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				if (finalFileName.length() > 200) {
					finalFileName = finalFileName.substring(finalFileName.length() - 200);
				}
				return finalFileName;
			}
		}
		return "unknown";
	}

	// Utility method
	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception ex) {
				throw new RuntimeException(ex.getMessage() + ". Ensure to properly setup UPLOAD_FOLDER application variable");
			}
		}
		FileOutputStream fop = null;
		try {
			fop = new FileOutputStream(file);
			fop.write(content);
			fop.flush();
		} catch (Exception ex) {

		} finally {
			fop.close();
		}
	}
}
