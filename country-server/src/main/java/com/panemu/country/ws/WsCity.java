package com.panemu.country.ws;

import com.panemu.country.common.CommonUtil;
import com.panemu.country.common.TableData;
import com.panemu.country.dto.DtoCity;
import com.panemu.country.dto.DtoCountryOption;
import com.panemu.country.error.ErrorCode;
import com.panemu.country.rcd.City;
import com.panemu.country.rcd.CountryData;
import com.panemu.country.rpt.RptCityList;
import com.panemu.country.security.NeedPermissions;
import com.panemu.country.srv.SrvCity;
import com.panemu.country.srv.SrvCountry;
import com.panemu.search.SortType;
import com.panemu.search.SortingInfo;
import com.panemu.search.TableCriteria;
import com.panemu.search.TableQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mubin
 */
@Path("/city")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class WsCity {

	private Logger log = LoggerFactory.getLogger(WsCity.class);

	@Inject
	private SrvCity srvCity;
	@Inject
	private SrvCountry srvCountry;

	@POST
	@NeedPermissions(value = {"city:read", "city:write"}, logical = Logical.OR)
	public TableData<DtoCity> findAll(@QueryParam("start") int startIndex, @QueryParam("max") int maxRecord, TableQuery tq) {
		Subject subject = SecurityUtils.getSubject();
		log.info("subject: " + subject.getPrincipal());
		TableData<City> lstResult = srvCity.find(tq, startIndex, maxRecord);
		List<DtoCity> lst = new ArrayList<>();
		lstResult.getRows().forEach(row -> {
			lst.add(DtoCity.create(row));
		});
		TableData<DtoCity> result = new TableData<>(lst, lstResult.getTotalRows());
		return result;
	}

	@GET
	@Path("countryList")
	@NeedPermissions(value = {"city:read", "city:write"}, logical = Logical.OR)
	public List<DtoCountryOption> getCountryByContinent(@QueryParam("continent") String continent) {
		TableQuery tq = new TableQuery();
		if (StringUtils.isNotBlank(continent)) {
			tq.getTableCriteria().add(new TableCriteria("continent", "\"" + continent + "\""));//use quote for exact match
		}
		tq.getSortingInfos().add(new SortingInfo("name", SortType.asc));
		TableData<CountryData> lstResult = srvCountry.find(tq, 0, 1000);

		List<DtoCountryOption> lstDto = lstResult.getRows().stream().map(item -> DtoCountryOption.create(item)).collect(Collectors.toList());
		return lstDto;
	}

	@GET
	@Path("{id}")
	@NeedPermissions(value = {"city:read", "city:write"}, logical = Logical.OR)
	public DtoCity getCity(@PathParam("id") Integer id) {
		DtoCity dto = null;
		if (id > 0) {
			City rcd = srvCity.findById(City.class, id);
			dto = DtoCity.create(rcd);
		}
		return dto;
	}

	@PUT
	@Path("{id}")
	@NeedPermissions(value = {"city:write"})
	public Integer saveCity(@PathParam("id") Integer id, DtoCity dto) {
		City city = null;
		if (id > 0) {
			city = srvCity.findById(City.class, id);
			srvCity.detach(city);//detach it to enable optimistic locking
		} else {
			city = new City();
		}
		city.setName(dto.name);
		city.setCountryDataid(dto.countryId);
		city.setVersion(dto.version);
		if (id > 0) {
			srvCity.update(city);
			return city.getId();
		} else {
			City saved = srvCity.insert(city);
			return saved.getId();
		}

	}

	@DELETE
	@Path("{id}")
	@NeedPermissions(value = {"city:write"})
	public void delete(@PathParam("id") Integer id) {
		City rcd = srvCity.findById(City.class, id);
		srvCity.delete(rcd);
	}

	@POST
	@Path("xls")
	@NeedPermissions(value = {"city:read", "city:write"}, logical = Logical.OR)
	public Response export(
			  @QueryParam("start") int startIndex,
			  @QueryParam("max") int maxRecord,
			  TableQuery tq) {
		TableData<DtoCity> data = this.findAll(startIndex, maxRecord, tq);
		RptCityList rpt = new RptCityList(data, startIndex, maxRecord);
		return CommonUtil.buildExcelResponse(rpt, "city");
	}

	@GET
	@Path("errornoparam")
	public void testError() {
		throw ErrorCode.ER0001.exception();
	}

	@GET
	@Path("errorwithparam")
	public void testError2() {
		String name = "Tomi";
		int age = 25;
		throw ErrorCode.ER0002.exception(name, age + "");
	}

	@GET
	@Path("unhandled_error")
	public void unHandledError() {
		String[] abc = new String[]{};
		String itWillTriggerError = abc[4];
	}

	@GET
	@Path("unauthorized_error")
	@NeedPermissions("you_dont_have_this_permission")
	public void unauthorizedError() {

	}
}
