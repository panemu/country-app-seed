package com.panemu.country.dto;

/**
 *
 * @author Nur Mubin <nur.mubin@panemu.com>
 */
public class DtoUserTemplate {

	public int id;
	public String username;
	public String email;
	public String password;
	public String idRole;
	public int version;
}
