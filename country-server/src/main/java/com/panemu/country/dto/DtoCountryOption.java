package com.panemu.country.dto;

import com.panemu.country.rcd.CountryData;

/**
 *
 * @author amrullah
 */
public class DtoCountryOption {
	public Integer id;
	public String name;
	
	public static DtoCountryOption create(CountryData rcd) {
		DtoCountryOption dto = new DtoCountryOption();
		dto.id = rcd.getId();
		dto.name = rcd.getName();
		return dto;
	}
}
