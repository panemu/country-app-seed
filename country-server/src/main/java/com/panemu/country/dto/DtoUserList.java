package com.panemu.country.dto;

import com.panemu.country.rcd.Member;
import java.util.Date;
import javax.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Nur Mubin <nur.mubin@panemu.com>
 */
public class DtoUserList {

	private static Logger log = LoggerFactory.getLogger(DtoUserList.class);
	public int id;
	public String username;
	public String email;
	public String password;
	public String role;
	public int version;
	public Date modifiedDate;
	
	public static DtoUserList create(Member rcd) {
		DtoUserList dto = new DtoUserList();
		dto.id = rcd.getId();
		dto.username = rcd.getUsername();
		dto.email = rcd.getEmail();
		dto.role = rcd.getRole();
		dto.modifiedDate = rcd.getModifiedDate();
		if (dto.version != 0) {
			try {
				dto.version = rcd.getVersion();
			} catch (EntityNotFoundException ex) {
				log.error(ex.getMessage());
			}
		}
		return dto;
	}
}
