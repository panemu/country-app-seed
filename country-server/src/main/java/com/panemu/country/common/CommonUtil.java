package com.panemu.country.common;

import com.panemu.country.rpt.RptAbstractXlsx;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@Singleton
@Startup
public class CommonUtil {

	private static Logger log = LoggerFactory.getLogger(CommonUtil.class);
	private static boolean DEV_MODE = false;
	private static String WS_DATE_FORMAT = "yyyy-MM-dd";
	private static String WS_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm";
	private static String WS_TIMESTAMP_SECOND_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static String SALT;
	@Resource(lookup = "java:global/country/salt")
	private String _salt;

	static {
		URL devUrl = CommonUtil.class.getResource("/development.txt");
		DEV_MODE = devUrl != null;
	}

	@PostConstruct
	public void initialize() {
		log.info("startingup...");
		SALT = this._salt;
	}
	
	public static void setSalt(String salt) {
		SALT = salt;
	}

	public static boolean isDevMode() {
		return DEV_MODE;
	}

	public static String getExtensionWithDot(String filename) {
		int dotIndex = filename.lastIndexOf(".");
		if (dotIndex < 0) {
			throw new RuntimeException("File has no extension: " + filename);
		}
		return filename.substring(dotIndex);
	}

	public static boolean isImageExtension(String ext) {
		ext = ext.replace(".", "");
		return ext.equalsIgnoreCase("jpg")
				  || ext.equalsIgnoreCase("jpeg")
				  || ext.equalsIgnoreCase("gif")
				  || ext.equalsIgnoreCase("png")
				  || ext.equalsIgnoreCase("tiff")
				  || ext.equalsIgnoreCase("bmp");
	}

	public static String getExtension(String filename) {
		int dotIndex = filename.lastIndexOf(".");
		if (dotIndex < 0) {
			throw new RuntimeException("File has no extension: " + filename);
		}
		return filename.substring(dotIndex + 1);
	}

	public static Date toDate(String yyyy_MM_dd) {
		if (StringUtils.isNotBlank(yyyy_MM_dd)) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(WS_DATE_FORMAT);
				return sdf.parse(yyyy_MM_dd);
			} catch (ParseException ex) {
				log.error("Failed to parse " + yyyy_MM_dd + " as date. " + ex.getMessage(), ex);
				return null;
			}
		}
		return null;
	}

	public static String toString_yyyy_MM_dd(Date date) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(WS_DATE_FORMAT);
		return sdf.format(date);
	}

	public static Date toTimeStamp(String yyyy_MM_dd_HH_mm_ss) {
		if (StringUtils.isNotBlank(yyyy_MM_dd_HH_mm_ss)) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(WS_TIMESTAMP_FORMAT);
				return sdf.parse(yyyy_MM_dd_HH_mm_ss);
			} catch (ParseException ex) {
				log.error("Failed to parse " + yyyy_MM_dd_HH_mm_ss + " as date. " + ex.getMessage(), ex);
				return null;
			}
		}
		return null;
	}

	public static Date toTimeStampWithSecond(String yyyy_MM_dd_HH_mm_ss) {
		if (StringUtils.isNotBlank(yyyy_MM_dd_HH_mm_ss)) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(WS_TIMESTAMP_SECOND_FORMAT);
				return sdf.parse(yyyy_MM_dd_HH_mm_ss);
			} catch (ParseException ex) {
				log.error("Failed to parse " + yyyy_MM_dd_HH_mm_ss + " as date. " + ex.getMessage(), ex);
				return null;
			}
		}
		return null;
	}

	public static String timeToString(Date time) {
		if (time != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(WS_TIMESTAMP_FORMAT);
			return sdf.format(time);
		}
		return "";
	}

	public static String hashText(String text) {
		String pwd = new Md5Hash(text).toHex();
		pwd = StringUtils.reverse(pwd);
		return pwd;
	}

	public static Response buildExcelResponse(RptAbstractXlsx dto, String fileName) {
		StreamingOutput streamingOutput = dto::export;
		DateFormat df = new SimpleDateFormat("yyyyMMdd_hhmm");
		String date = df.format(new Date());
		return Response.ok(streamingOutput, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet").header("Content-Disposition", "attachment; filename=" + fileName + "_" + date + ".xlsx").build();
	}

	public static String digestToken(String token) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.reset();
			digest.update(SALT.getBytes(StandardCharsets.UTF_8));
			byte[] hash = digest.digest(token.getBytes(StandardCharsets.UTF_8));
			String encoded = Base64.getEncoder().encodeToString(hash);
			return encoded;
		} catch (NoSuchAlgorithmException ex) {
			throw new RuntimeException(ex);
		}
	}
}
