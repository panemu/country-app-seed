package com.panemu.country.security;

import java.io.IOException;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author amrullah
 */
@NeedAuthentication
@Provider
@Priority(Priorities.AUTHENTICATION)
public class NeedAuthenticationFilter implements ContainerRequestFilter {


	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		Subject subject = SecurityUtils.getSubject();
		if (!subject.isAuthenticated()) {
			throw new UnauthenticatedException( "The current Subject is not authenticated.  Access denied." );
		}
	}

}
