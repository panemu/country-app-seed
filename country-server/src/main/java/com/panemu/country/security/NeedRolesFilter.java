package com.panemu.country.security;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@NeedRoles("")
@Provider
@Priority(Priorities.AUTHORIZATION)
public class NeedRolesFilter implements ContainerRequestFilter {

	private static Logger log = LoggerFactory.getLogger(NeedPermissionsFilter.class);

	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		Method resourceMethod = resourceInfo.getResourceMethod();
		NeedRoles rrAnnotation = resourceMethod.getAnnotation(NeedRoles.class);
		if (rrAnnotation != null) {
			String[] roles = rrAnnotation.value();

			Subject subject = SecurityUtils.getSubject();
			if (roles.length == 1) {
				subject.checkRole(roles[0]);
				return;
			}
			if (Logical.AND.equals(rrAnnotation.logical())) {
				subject.checkRoles(Arrays.asList(roles));
				return;
			}
			if (Logical.OR.equals(rrAnnotation.logical())) {
				// Avoid processing exceptions unnecessarily - "delay" throwing the exception by calling hasRole first
				boolean hasAtLeastOneRole = false;
				for (String role : roles) {
					if (subject.hasRole(role)) {
						hasAtLeastOneRole = true;
						break;
					}
				}
				// Cause the exception if none of the role match, note that the exception message will be a bit misleading
				if (!hasAtLeastOneRole) {
					subject.checkRole(roles[0]);
				}
			}
		}
	}

}
