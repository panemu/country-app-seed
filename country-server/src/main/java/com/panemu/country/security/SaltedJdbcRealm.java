package com.panemu.country.security;

import com.panemu.country.common.CommonUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.JdbcUtils;

/**
 *
 * @author amrullah
 */
public class SaltedJdbcRealm extends JdbcRealm {

	private static SaltedJdbcRealm instance;

	public SaltedJdbcRealm() {
		instance = this;
		setSaltStyle(SaltStyle.COLUMN);
		setSaltIsBase64Encoded(false);
	}

	public AuthorizationInfo getSubjectInfo(Subject subject) {
		return this.doGetAuthorizationInfo(subject.getPrincipals());
	}

	public static SaltedJdbcRealm getInstance() {
		return instance;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		if (token instanceof ShiroAuthToken) {
			try {
				String digestToken = CommonUtil.digestToken(token.getPrincipal() + "");
				String username = getUsername(digestToken);
				if (username == null || username.trim().isEmpty()) {
					throw new AuthenticationException("token invalid");
				}
				return new SimpleAuthenticationInfo(username, digestToken, "token_realm");
			} catch (SQLException e) {
				final String message = "There was a SQL error while authenticating user";
				throw new AuthenticationException(message, e);
			}
		}
		return super.doGetAuthenticationInfo(token); //To change body of generated methods, choose Tools | Templates.
	}

	private String getUsername(String digestToken) throws SQLException {
		Connection conn = dataSource.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String username = null;
		try {
			ps = conn.prepareStatement("select username from member where auth_token = ?");
			ps.setString(1, digestToken);

			// Execute query
			rs = ps.executeQuery();

			// Loop over results - although we are only expecting one result, since usernames should be unique
			boolean foundResult = false;
			while (rs.next()) {
				
				// Check to ensure only one row is processed
				if (foundResult) {
					throw new AuthenticationException("More than one user row found for user [" + username + "]. Usernames must be unique.");
				}

				username = rs.getString(1);

				foundResult = true;
			}
		} finally {
			JdbcUtils.closeResultSet(rs);
			JdbcUtils.closeStatement(ps);
			JdbcUtils.closeConnection(conn);
		}
		return username;
	}

	public boolean supports(AuthenticationToken token) {
		return token != null && (getAuthenticationTokenClass().isAssignableFrom(token.getClass()) || ShiroAuthToken.class.isAssignableFrom(token.getClass()));
	}

	@Override
	protected void assertCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) throws AuthenticationException {
		if (getAuthenticationTokenClass().isAssignableFrom(token.getClass())) {
			super.assertCredentialsMatch(token, info);
		}
	}
	
}
