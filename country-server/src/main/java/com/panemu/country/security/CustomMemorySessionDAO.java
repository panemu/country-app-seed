package com.panemu.country.security;

import java.io.Serializable;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;

/**
 *
 * @author amrullah
 */
public class CustomMemorySessionDAO extends MemorySessionDAO{

	@Override
	protected Session doReadSession(Serializable sessionId) {
		String ssid = sessionId.toString();
		int dotIndex = ssid.indexOf(".");
		if (dotIndex > -1) {
			ssid = ssid.substring(0, dotIndex);
		}
		return super.doReadSession(ssid);
	}
	
}
