package com.panemu.country.security;

import org.apache.shiro.authc.AuthenticationToken;

/**
 *
 * @author amrullah
 */
public class ShiroAuthToken implements AuthenticationToken {

	private final String authToken;

	public ShiroAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	@Override
	public Object getPrincipal() {
		return authToken;
	}

	@Override
	public Object getCredentials() {
		return authToken;
	}
	
}
