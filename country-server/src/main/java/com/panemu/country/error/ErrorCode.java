package com.panemu.country.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
public enum ErrorCode {
	/**
	 * This is generic error code. It accept a string for the error message. Since it is generic, it will not be translated in client side.
	 * You can use it this way:
	 * throw ErrorCode.ER0000.exception("Pesan error generic.");
	 */
	ER0000("%s"),
	ER0001("This is an example of error message. If you choose other language, this error is also translated."),
	ER0002("Error Message With Parameters. Name: %s. Age: %s"),
	ER0003("Old password doesn't match"),
	ER0004("Old password can't be empty!"),
	ER0005("Email already exists, please use another one"),
	ER0006("Can't delete admin account"),
	ER0007("Can't delete your own account"),
	ER0008("Can't change password of username hellocountry!"),
	ER0009("%s already used, please use another"),
	ER0010("Invalid search criteria [%s : %s]"),
	/**
	 * Your data has been updated by other user. Please refresh!
	 */
	ER0100("Your data has been updated by other user. Please refresh!"),
	ER0400("Bad Request. %s"),
	/**
	 * You are not authenticated. Please login.
	 */
	ER0401("You are not authenticated. Please login."),
	/**
	 * You don't have access to the requested action. Please contact
	 * Administrator.
	 */
	ER0403("You don't have access to the requested action. Please contact Administrator.");

	private final String message;
	private static Logger log = LoggerFactory.getLogger(ErrorCode.class);
	private ErrorCode(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	public ErrorEntity create(String... parameters) {
		ErrorEntity ee = new ErrorEntity(this, parameters);
		return ee;
	}
	
	public KnownException exception(String... parameters) {
		ErrorEntity errorEntity = this.create(parameters);
		log.error(errorEntity.getMessage());
		final KnownException badRequestException = new KnownException(Response.status(Response.Status.BAD_REQUEST)
				  .entity(errorEntity).type(MediaType.APPLICATION_JSON).build());
		return badRequestException;
		
	}
}
