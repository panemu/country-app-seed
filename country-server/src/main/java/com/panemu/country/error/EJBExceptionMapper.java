package com.panemu.country.error;

import javax.ejb.EJBException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@Provider
public class EJBExceptionMapper implements ExceptionMapper<EJBException> {

	private Logger log = LoggerFactory.getLogger(EJBExceptionMapper.class);
	@Context
	private HttpServletRequest request;

	@Override
	public Response toResponse(EJBException exception) {
		String requestInfo = request.getMethod() + " " + request.getPathInfo();
		if (request.getQueryString() != null) {
			requestInfo = requestInfo + "?" + request.getQueryString();
		}
		
		Throwable cause = exception.getCause() != null ? exception.getCause() : exception;
		
		String message = requestInfo + " " +  cause.getClass().getName()+ " : " + cause.getMessage();
		log.error(requestInfo);
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(message).type(MediaType.APPLICATION_JSON).build();
	}

}
