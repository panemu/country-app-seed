package com.panemu.country.error;

import javax.ejb.ApplicationException;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@ApplicationException(rollback = false)
public class NonRollbackException extends BadRequestException {
	private static Logger log = LoggerFactory.getLogger(NonRollbackException.class);
	public NonRollbackException(Response response) {
		super(response);
	}
	
//	public static NonRollbackException createSMSError() {
//		ErrorEntity ee = new ErrorEntity(ErrorCode.ER0046, "");
//		final NonRollbackException badRequestException = new NonRollbackException(Response.status(Response.Status.BAD_REQUEST)
//				  .entity(ee).type(MediaType.APPLICATION_JSON).build());
//		return badRequestException;
//	}
}
