package com.panemu.country.srv;

import com.panemu.country.common.TableData;
import com.panemu.country.error.ErrorCode;
import com.panemu.country.rcd.CountryData;
import com.panemu.search.InvalidSearchCriteria;
import com.panemu.search.SortingInfo;
import com.panemu.search.TableCriteria;
import com.panemu.search.TableQuery;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mubin
 */
@Stateless
public class SrvCountry extends SrvBase {

	private Logger log = LoggerFactory.getLogger(SrvCountry.class);
	@PersistenceContext
	protected EntityManager em;

	public CountryData insert(CountryData rcd) {
		return super.insert(rcd);
	}

	public CountryData update(CountryData rcd) {
		return super.update(rcd);
	}

	public void delete(CountryData rcd) {
		super.delete(rcd);
	}

	public TableData<CountryData> find(TableQuery query, int startIndex, int maxRecord) {
		if (query == null) {
			query = new TableQuery();
		}
		try {
			for (TableCriteria crit : query.getTableCriteria()) {
				crit.setTableAlias("ui");
				if ("country".equals(crit.getAttributeName())) {
					crit.setAttributeName("id");
					crit.setSearchModeToInt();
				}
				if ("independence".equals(crit.getAttributeName())) {
					crit.setSearchModeToDate();
				}
				if ("population".equals(crit.getAttributeName())) {
					crit.setSearchModeToBigInteger();
				}
			}
			for (SortingInfo si : query.getSortingInfos()) {
				si.setTableAlias("ui");
			}

			String whereClause = query.generateWhereClause(true);
			String orderClause = query.generateOrderByClause(true);
			TypedQuery<Long> countQuery = em.createQuery("SELECT count(ui) from CountryData ui " + whereClause, Long.class);
			query.applyParameter(countQuery);
			long totalRow = countQuery.getSingleResult();

			TypedQuery<CountryData> typedQuery = em.createQuery("SELECT ui from CountryData ui " + whereClause + orderClause, CountryData.class);
			query.applyParameter(typedQuery);
			typedQuery.setFirstResult(startIndex);
			if (maxRecord > 0) {
				typedQuery.setMaxResults(maxRecord);
			}
			List<CountryData> resultList = typedQuery.getResultList();
			TableData<CountryData> td = new TableData<>(resultList, totalRow);
			return td;
		} catch (InvalidSearchCriteria ex) {
			throw ErrorCode.ER0010.exception(ex.getColumnName(), ex.getValue());
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw ErrorCode.ER0400.exception(ex.getClass().getSimpleName());
		}
	}
}
