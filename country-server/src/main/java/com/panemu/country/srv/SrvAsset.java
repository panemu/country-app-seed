package com.panemu.country.srv;

import com.panemu.country.common.TableData;
import com.panemu.country.error.ErrorCode;
import com.panemu.country.rcd.Asset;
import com.panemu.search.SortingInfo;
import com.panemu.search.TableCriteria;
import com.panemu.search.TableQuery;
import java.io.File;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@Stateless

public class SrvAsset extends SrvBase {

	@PersistenceContext
	private EntityManager em;
	private Logger log = LoggerFactory.getLogger(SrvAsset.class);
	@Resource(lookup = "java:global/country/upload-folder")
	private String uploadFolder;
//	@Inject
//	private SrvSetting srvSetting;

	public void deleteAsset(Asset asset) {
		if (asset == null) {
			return;
		}
		log.debug("delete asset " + asset.getId());
		super.delete(asset);
		if (!uploadFolder.endsWith(File.separator)) {
			uploadFolder = uploadFolder + File.separator;
		}
		String fileName = uploadFolder + asset.getCategory() + File.separator + asset.getPath();
		String fileNameSmall = uploadFolder + asset.getCategory() + File.separator + asset.getPathSmall();
		File file = new File(fileName);
		if (file.exists()) {
			file.delete();
		}
		File fileSmall = new File(fileNameSmall);
		if (fileSmall.exists()) {
			fileSmall.delete();
		}
	}
	
	public Asset update(Asset asset) {
		return super.update(asset);
	}
	
	public Asset insert(Asset asset) {
		return super.insert(asset);
	}

	public void deleteAsset(String category, int refId) {
		List<Asset> lstAsset = getAssets(category, refId);
		for (Asset asset : lstAsset) {
			deleteAsset(asset);
		}
	}

	private void supplySizeInfo(List<Asset> assets) {
		if (!uploadFolder.endsWith(File.separator)) {
			uploadFolder = uploadFolder + File.separator;
		}
		for (Asset asset : assets) {
			File f = new File(uploadFolder + asset.getCategory() + "/" + asset.getPath());
			long size = f.exists() ? f.length() : 0;
			asset.setSize(size);
		}
	}

	public List<Asset> getAssets(String category, int refId) {
		String sql = "SELECT a from Asset a WHERE a.refId=:refId and a.category=:category ";
		TypedQuery<Asset> tq = em.createQuery(sql, Asset.class);
		tq.setParameter("refId", refId);
		tq.setParameter("category", category);
		List<Asset> lstResult = tq.getResultList();
		supplySizeInfo(lstResult);
		return lstResult;
	}

	public List<Asset> getAssetToOverwrite(String category, int refId, String subCategory) {

		String sql = "SELECT a from Asset a WHERE a.refId=:refId and a.category=:category ";
		if (StringUtils.isNotBlank(subCategory)) {
			sql = sql + " AND a.subCategory = :subCategory ";
		} else {
			sql = sql + " AND (a.subCategory is null OR a.subCategory = '') ";
		}

		TypedQuery<Asset> tq = em.createQuery(sql, Asset.class);
		tq.setParameter("refId", refId);
		tq.setParameter("category", category);
		if (StringUtils.isNotBlank(subCategory)) {
			tq.setParameter("subCategory", subCategory);
		}
		List<Asset> result = tq.getResultList();
//			if (result != null) {
//				supplySizeInfo(Arrays.asList(result));
//			}
		return result;
	}

	public List<Asset> getAssets(String category, List<Integer> lstRefId) {
		TypedQuery<Asset> tq = em.createQuery("SELECT a from Asset a where a.category= :category and a.refId IN (:lstId)", Asset.class);
		tq.setParameter("category", category);
		tq.setParameter("lstId", lstRefId);
		
		return tq.getResultList();
	}
	
	public TableData<Asset> find(TableQuery query, int startIndex, int maxRecord) {
		try {
			for (TableCriteria crit : query.getTableCriteria()) {
				crit.setTableAlias("ui");
			}
			for (SortingInfo si : query.getSortingInfos()) {
				si.setTableAlias("ui");
			}

			String whereClause = query.generateWhereClause(true);
			String orderClause = query.generateOrderByClause(true);
			TypedQuery<Long> countQuery = em.createQuery("SELECT count(ui) from Asset ui " + whereClause, Long.class);
			query.applyParameter(countQuery);
			long totalRow = countQuery.getSingleResult();

			TypedQuery<Asset> typedQuery = em.createQuery("SELECT ui from Asset ui " + whereClause + orderClause, Asset.class);
			query.applyParameter(typedQuery);
			typedQuery.setFirstResult(startIndex);
			if (maxRecord > 0) {
				typedQuery.setMaxResults(maxRecord);
			}
			List<Asset> resultList = typedQuery.getResultList();
			TableData<Asset> td = new TableData<>(resultList, totalRow);
			return td;
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw ErrorCode.ER0400.exception(ex.getClass().getSimpleName());
		}
	}


}
